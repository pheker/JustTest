package cn.pheker.designpattern.adapter;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/5/5 0:47
 * email     1176479642@qq.com
 * desc     电脑(外接适配器): 适配器模式之类适配器
 *
 * </pre>
 */
public class Computer implements ISource {

    private ISource adapter;

    private Computer(ISource adapter) {
        this.adapter = adapter;
    }


    public static Computer useAdapter(ISource adapter) {
        Computer computer = new Computer(adapter);
        return computer;
    }

    @Override
    public void charge() {
        adapter.charge();
    }

}
