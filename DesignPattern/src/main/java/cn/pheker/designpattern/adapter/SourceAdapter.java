package cn.pheker.designpattern.adapter;

import cn.pheker.utils.UtilLogger;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/5/5 1:10
 * email     1176479642@qq.com
 * desc     电源适配器
 *
 * </pre>
 */
public class SourceAdapter extends CommonAdapter {

    private CommonSource source;

    SourceAdapter(CommonSource source) {
        this.source = source;
    }

    /**
     * 接入电源
     * @param source 电源
     * @return  适配器
     */
    public static IAdapter useSource(CommonSource source) {
        SourceAdapter adapter = new SourceAdapter(source);
        return adapter;
    }

    @Override
    public void charge() {
        int newVoltage = convertVoltage(source.getVoltage());
        UtilLogger.println(newVoltage+source.getUnit() + source.showCharging());
    }

    @Override
    public int convertVoltage(int voltage) {
        if (voltage == 220) {
            int newVoltage = 36;
            UtilLogger.println(showConverted(voltage,newVoltage));
            return newVoltage;
        }
        return super.convertVoltage(voltage);
    }
}
