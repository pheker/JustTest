package cn.pheker.designpattern.adapter;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/5/5 13:04
 * email     1176479642@qq.com
 * desc     通用电源
 *
 * </pre>
 */
public abstract class CommonSource implements ISource {
    /**
     * 电压
     */
    private int voltage = 220;
    /**
     * 单位
     */
    private String unit = "V";
    /**
     * 单位全称
     */
    private String fullUnit = "volt";

    private String changing = " -> 正在充电...";

    public String showVoltageWithVolt() {
        return voltage+unit;
    }

    public int getVoltage() {
        return this.voltage;
    }

    public String getUnit() {
        return unit;
    }

    public String showCharging() {
        return changing;
    }
}
