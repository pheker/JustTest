package cn.pheker.designpattern.adapter;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/5/5 12:34
 * email     1176479642@qq.com
 * desc     电脑(自带适配器)：适配器模式之对象适配器
 *
 * </pre>
 */
public class ComputerWidthAdapter extends SourceAdapter {

    private ComputerWidthAdapter(CommonSource source) {
        super(source);
    }

    public static ComputerWidthAdapter useSource(CommonSource source) {
        ComputerWidthAdapter computerWidthAdapter = new ComputerWidthAdapter(source);
        return computerWidthAdapter;
    }

    public void  charge() {
        super.charge();
    }


}
