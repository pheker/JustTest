package cn.pheker.designpattern.adapter;

import cn.pheker.utils.UtilLogger;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/5/5 1:14
 * email     1176479642@qq.com
 * desc
 *
 * </pre>
 */
public class TestAdapter {

    public static void main(String[] args) {
        testClassAdapter();
        testOjbectAdapter();
    }

    private static void testOjbectAdapter() {
        UtilLogger.printSplitLine("适配器模式之对象适配器");
        Source source = new Source();
        ComputerWidthAdapter computerWidthAdapter = ComputerWidthAdapter.useSource(source);
        computerWidthAdapter.charge();
    }

    private static void testClassAdapter() {
        UtilLogger.printSplitLine("适配器模式之类适配器");
        Source source = new Source();
        SourceAdapter adapter = (SourceAdapter) SourceAdapter.useSource(source);
        Computer computer = Computer.useAdapter(adapter);
        computer.charge();
    }

}
