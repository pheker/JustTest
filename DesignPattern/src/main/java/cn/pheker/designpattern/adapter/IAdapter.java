package cn.pheker.designpattern.adapter;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/5/5 14:54
 * email     1176479642@qq.com
 * desc
 *
 * </pre>
 */
public interface IAdapter extends ISource {
}
