package cn.pheker.designpattern.adapter;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/5/5 12:57
 * email     1176479642@qq.com
 * desc     电源接口
 *          功能：充电
 *
 * </pre>
 */
public interface ISource {

    /**
     * 充电
     */
    void charge();

}
