package cn.pheker.designpattern.adapter;

import cn.pheker.utils.UtilLogger;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/5/5 0:43
 * email     1176479642@qq.com
 * desc     电源类
 *
 * </pre>
 */
public class Source extends CommonSource{

    @Override
    public void charge() {
        UtilLogger.println(showVoltageWithVolt()+showCharging());
    }

}
