package cn.pheker.designpattern.adapter;

import cn.pheker.utils.UtilString;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/5/5 13:43
 * email     1176479642@qq.com
 * desc
 *
 * </pre>
 */
public abstract class CommonAdapter implements IAdapter {

    /**
     * 转换电压
     * @param voltage 原电压
     * @return  转换后的电压
     */
    public int convertVoltage(int voltage) {
        return voltage;
    }

    /**
     * 显示转换电压字符
     * @param voltage 原电压
     * @param newVoltage 新电压
     * @return  转换字符
     */
    public String showConverted(int voltage, int newVoltage) {
        return UtilString.format("转换电压({} => {})",voltage,newVoltage);
    }
}
