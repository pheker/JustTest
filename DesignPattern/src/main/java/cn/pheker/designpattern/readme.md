
# 设计模式(23种)
# 1.分类
* 建造型(5种)
> 单例，工厂，抽象工厂，建造者，原型
* 结构型(7种)
* 行为型(11种)

# 2.场景
1.适配器 用于功能转换
2.装饰器 用于添加新功能
