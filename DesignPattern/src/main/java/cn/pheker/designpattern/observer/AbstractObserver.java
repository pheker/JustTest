package cn.pheker.designpattern.observer;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/5/5 20:27
 * email     1176479642@qq.com
 * desc
 *
 * </pre>
 */
public abstract class AbstractObserver implements Observer {

    Subject subject;
    @Override
    public void update() {

    }
}
