package cn.pheker.designpattern.observer;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/5/5 20:25
 * email     1176479642@qq.com
 * desc
 *
 * </pre>
 */
public interface Observer {
    void update();
}
