package cn.pheker.designpattern.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/5/5 20:18
 * email     1176479642@qq.com
 * desc
 *
 * </pre>
 */
public class Subject {
    private int state;
    private List<Observer> observers = new ArrayList<>();

    public Subject(int state) {
        this.state = state;
    }

    public void attach(Observer observer) {
        observers.add(observer);
    }

    public void notifyAllObservers(){
        for (Observer observer : observers) {
            observer.update();
        }
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

}
