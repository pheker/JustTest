package cn.pheker.designpattern.observer;

import cn.pheker.utils.UtilLogger;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/5/5 20:31
 * email     1176479642@qq.com
 * desc
 *
 * </pre>
 */
public class ObserverFactory {

    public static Observer build(int radix, Subject subject) {
        Observer observer;
        switch (radix) {
            case 2:
                observer = new BinaryObserver(subject);
                break;
            case 8:
                observer = new OctonaryObserver(subject);
                break;
            case 16:
                observer = new HexadecimalObserver(subject);
                break;
            default:
                observer = new OtherRadixObserver(subject,radix);
                break;
        }
        return observer;
    }

    static class BinaryObserver extends AbstractObserver{

        public BinaryObserver(Subject subject) {
            this.subject = subject;
            this.subject.attach(this);
        }

        @Override
        public void update() {
            UtilLogger.println("Binary String:"+Integer.toBinaryString(subject.getState()));
        }
    }

    static class OctonaryObserver extends AbstractObserver{

        public OctonaryObserver(Subject subject) {
            this.subject = subject;
            this.subject.attach(this);
        }

        @Override
        public void update() {
            UtilLogger.println("Octonary String:"+Integer.toOctalString(subject.getState()));
        }
    }

    static class HexadecimalObserver extends AbstractObserver{

        public HexadecimalObserver(Subject subject) {
            this.subject = subject;
            this.subject.attach(this);
        }

        @Override
        public void update() {
            UtilLogger.println("Hexadecimal String:"+Integer.toHexString(subject.getState()));
        }
    }

    static class OtherRadixObserver extends AbstractObserver{
        private int radix;
        public OtherRadixObserver(Subject subject, int radix) {
            this.subject = subject;
            this.subject.attach(this);
            this.radix = radix;
        }

        @Override
        public void update() {
            UtilLogger.println(radix+" Radix String:"+Integer.toString(subject.getState(),radix));
        }
    }


}
