package cn.pheker.designpattern.observer;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/5/5 20:57
 * email     1176479642@qq.com
 * desc
 *
 * </pre>
 */
public class TestObserver {

    public static void main(String[] args) {
        test();
    }

    private static void test() {
        Subject subject = new Subject(17);
        Observer ob2 = ObserverFactory.build(2, subject);
        Observer ob8 = ObserverFactory.build(8, subject);
        Observer ob16 = ObserverFactory.build(16, subject);
        Observer ob9 = ObserverFactory.build(11, subject);
        subject.notifyAllObservers();

    }

}
