package cn.pheker.utils;

import org.junit.Test;

/**
 * @author cn.pheker
 * @date 2017/11/25  17:40
 * @email 1176479642@qq.com
 * @desc    自定义加密工具
 */
public class UtilCipher {
    
    public static final String[] table = new String[]{//33
        "0","1","2","3","4","5","6","7","8","9","a","b",
        "c","d","e","f","g","h","i","j","k","l","m","n",
        "o","p","q","r","s","t","u","v","w","x","y","z"
    };
    
    public static String encode(String text,String pwd,boolean clearMulti0) {
        pwd = UtilMD5.MD5(pwd);
        String ret = toTwoByteArray(toBinaryStr(text + pwd), pwd).toUpperCase();
        return clearMulti0?ret.replaceAll("0+","0"):ret;
    }
    
    @Test
    public void testBatch() {
        int i = 0;
        while (i++ < 32) {
            String text = UtilString.repeat("0",i);
            UtilLogger.println("text: "+text+",encode: "+encode("",text,false));
        }
        while (i-- > 0) {
            String text = UtilString.repeat("0", i);
            UtilLogger.println("text: " + text + ",encode: " + encode(text, "", false));
        }
    }
    
    @Test
    public void testElapsedTime() {
        int i = 0;
        while (i++ < 32) {
            final String text = UtilString.repeat("0",i);
            UtilElapsedTime.test(new UtilElapsedTime.Callback() {
                @Override
                public Object compute() {
                    return encode("",text,false);
                }
            });
        }
    }
    
    /**
     * 转换为二进制字符串
     * @param str
     * @return
     */
    public static StringBuilder toBinaryStr(String str) {
        //非ascii码转换,to utf8
        
        //to binary
        StringBuilder sb = new StringBuilder();
        int len = str.length();
        for (int i = 0; i < len; i++) {
            sb.append(UtilBit.toBinaryStr(str.charAt(i)));
        }
        return sb;
    }
    
    public static String toTwoByteArray(StringBuilder sb, String pwdMd5) {
        StringBuilder retSb = new StringBuilder();
        int pwdCurr = 0;
        while (sb.length()>0) {
            //循环将突MD5的每一位转换为int(0-15位)
            String pwdCh = pwdMd5.substring(pwdCurr%32,pwdCurr%32+1);
            int cutEnd = UtilBit.hex2int(pwdCh);
            cutEnd = cutEnd <= sb.length()?cutEnd:sb.length();
            
            //将cutEnd作为截取长度
            String twoByte = sb.substring(0,cutEnd);
            twoByte = UtilString.repeat("0",16-twoByte.length())+twoByte;

            //将截取的字符转为int,作为获取table字符表的索引,并索引对应的字符返回
            retSb.append(table[UtilBit.toInteger(twoByte.substring(0,5))]);
            retSb.append(table[UtilBit.toInteger(twoByte.substring(5,10))]);
            retSb.append(table[UtilBit.toInteger(twoByte.substring(10,15))]);
            
//            retSb.append(UtilBit.toHexStr(UtilBit.toInteger(twoByte.substring(0,5)),false));
//            retSb.append(UtilBit.toHexStr(UtilBit.toInteger(twoByte.substring(5,10)),false));
//            retSb.append(UtilBit.toHexStr(UtilBit.toInteger(twoByte.substring(10,15)),false));

            pwdCurr++;
            //删除
            sb = sb.delete(0,cutEnd);
            
        }
        return retSb.toString();
    }
    
}
