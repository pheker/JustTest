package cn.pheker.utils;

/**
 * @author  cn.pheker
 * @date    2017/11/29  20:04
 * @email   1176479642@qq.com
 * @desc    异常工具类
 */
public class UtilException {
    /**
     * 获取异常详细信息
     * @param e
     * @return
     */
    public static String getExceptionDetail(Exception e) {
        StringBuffer stringBuffer = new StringBuffer(e.toString() + "\n");
        StackTraceElement[] messages = e.getStackTrace();
        int length = messages.length;
        for (int i = 0; i < length; i++) {
            stringBuffer.append("\t"+messages[i].toString()+"\n");
        }
        return stringBuffer.toString();
    }
    
    
}
