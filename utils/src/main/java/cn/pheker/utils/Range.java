package cn.pheker.utils;

import java.util.Iterator;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/3/16 13:47
 * email     1176479642@qq.com
 * desc      Range工具类
 *
 * </pre>
 */
public class Range  implements Iterable<Long>{
    
    private Stream<Long> range;
    
    public Range(final long length) {
        range = newRange(0, length, 1);
    }
    public Range(final long start, final long length) {
        range = newRange(start, length, 1);
    }
    public Range(final long start, final long length, final int step) {
        range = newRange(start, length, step);
    }
    
    public static Stream<Long> newRange(final long length){
        return newRange(0, length, 1);
    }
    
    public static Stream<Long> newRange(final long start, final long length){
        return newRange(start, length, 1);
    }
    
    public static Stream<Long> newRange(final long start, final long length, final int step){
        Supplier<Long> seed = new Supplier<Long>() {
            private long next = start;
            @Override
            public Long get() {
                long $next = next;
                next += step;
                return $next;
            }
        };
        return Stream.generate(seed).limit(length);
    }
    
    @Override
    public Iterator<Long> iterator() {
        return range.iterator();
    }
    
    public Stream<Long> build(){
        return range;
    }
    
}
