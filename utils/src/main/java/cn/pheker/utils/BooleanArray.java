package cn.pheker.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import java.util.function.Consumer;

import static cn.pheker.utils.UtilBit.setBitOne;
import static cn.pheker.utils.UtilBit.setBitZero;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/5/2 14:25
 * email     1176479642@qq.com
 * desc     Boolean数组，一个boolean用一个bit来存储
 *
 * </pre>
 */
public class BooleanArray{

    private volatile int sum = 0;
    private volatile ArrayList<Byte> bytes = new ArrayList<>();
    private Iterator<Boolean> ite = new BooleanArrayIterator(this);

    private BooleanArray(){}
    public BooleanArray(Boolean... bs) {
        BooleanArray instance = getInstance();
        for (boolean b : bs) {
            instance.push(b);
        }
    }

    public static BooleanArray getInstance(){
        return new BooleanArray();
    }

    /**
     * 追加到数组尾部,并返回其位置索引
     * @param b
     * @return index
     */
    public synchronized int push(boolean b) {
        int index = sum / Byte.SIZE, offset = sum % Byte.SIZE;
        if(sum == 0) offset = 0;
        byte by = 0x0;
        int bSize = bytes.size();
        if(index < bSize){
            by = bytes.get(index);
            by = (byte)(b? setBitOne(by, offset):setBitZero(by,offset));
            bytes.set(index, by);
        }else{
            by = (byte)(b? setBitOne(by, offset):setBitZero(by,offset));
            bytes.add(by);
        }
        sum++;
        return sum-1;
    }

    /**
     * 获取第index位boolean元素
     * @param index 索引
     * @return boolean元素
     */
    public boolean get(int index) {
        checkBounds(index);
        int i = index / Byte.SIZE, offset = index % Byte.SIZE;
        Byte by = bytes.get(i);
        int bit = by>>offset&1;
        return bit == 1 ? true : false;
    }

    /**
     * 将第index位设置为b
     * @param index 索引
     * @param b boolean
     * @return 是否成功
     */
    public synchronized boolean set(int index, boolean b) {
        checkBounds(index);
        return seti(index,b);
    }

    private boolean seti(int index, boolean b) {
        int i = index / Byte.SIZE, offset = index % Byte.SIZE;
        Byte by = bytes.get(i);
        by = (byte)(b? setBitOne(by, offset):setBitZero(by,offset));
        bytes.set(i, by);
        return true;
    }
    /**
     * 删除第index个boolean
     * @param index 索引
     * @return 是否成功
     */
    public synchronized boolean remove(int index) {
        checkBounds(index);
        if(index < sum -1) {
            for (int i = index; i < sum - 1; i++) {
                exchange(i, i + 1);
            }
        }
        boolean flag = seti(sum - 1, false);
        if(flag) {
            sum--;
        }
        return flag;
    }

    private void checkBounds(int index){
        if (index >= sum) {
            throw new RuntimeException("IndexOutOfBoundsException("+index+"/"+(sum-1)+")");
        }
    }
    private synchronized void exchange(int i, int j) {
//        if(i<j && j<sum){
            boolean bi = get(i);
            boolean bj = get(j);
            seti(j, bi);
            seti(i, bj);
    }

    public int size(){
        return sum;
    }

    public synchronized void clear(){
        bytes.clear();
        sum = 0;
    }

    @Override
    public String toString() {
        String ret = "[";
        StringBuilder sb = new StringBuilder();
        Iterator<Boolean> ite = iterator();
        while (ite.hasNext()) {
            sb.append(","+ite.next());
        }
        if (sb.length() > 0) {
            ret += sb.substring(1);
        }
        return ret + "]";
    }


    public void each(Consumer action) {
        Objects.requireNonNull(action);
        while (ite.hasNext())
            action.accept(ite.next());
    }

    public Iterator<Boolean> iterator(){
        return new BooleanArrayIterator(this);
    }


    /* iterator */
    class BooleanArrayIterator implements Iterator<Boolean> {
        private volatile int cursor = 0;
        private BooleanArray ba;
        private BooleanArrayIterator(BooleanArray ba){
            this.ba = ba;
        }
        @Override
        public boolean hasNext() {
            return ba.size() > cursor;
        }

        @Override
        public Boolean next() {
            boolean b = ba.get(cursor);
            cursor++;
            return b;
        }

        @Override
        public void remove() {
            ba.remove(cursor);
            cursor--;
        }
    }
}