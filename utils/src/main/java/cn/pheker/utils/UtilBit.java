package cn.pheker.utils;

import org.junit.Test;

/**
 * @author cn.pheker
 * @date 2017/11/23  10:38
 * @email 1176479642@qq.com
 * @desc    位操作工具类
 */
public class UtilBit {
    
    /**
     * 转为二进制字符串
     * @param m
     * @return
     */
    public static String toBinaryStr(int m) {
        StringBuilder sb = new StringBuilder();
        if (m == 0) {
            return "0";
        }
        for (int n=m; n>0; n >>=1) {
            sb.append(n % 2);
        }
        return sb.reverse().toString();
    }
    
    
    /**
     * int转为十六进制字符串
     * @param m
     * @return
     */
    public static String toHexStr(int m) {
        return toHexStr(m, true);
    }
    /**
     * int转为十六进制字符串
     * @param m
     * @param show0x 是否显示十六进制头
     * @return
     */
    public static String toHexStr(int m,boolean show0x) {
        m = m & 0x7FFFFFFF;//取绝对值
        StringBuilder sb = new StringBuilder();
        do {
            int temp = m & 15;  //15是掩码！
            char ch = (char) (temp > 9 ?
                    ("A".charAt(0) + temp - 10) :
                    ("0".charAt(0) + temp));
            sb.append(ch);
        } while ((m >>= 4)> 0);
        return (show0x?"0x":"")+sb.reverse().toString();
    }
    
    /**
     * 十六进制字符串转为
     * @param hexStr
     * @return
     */
    public static Integer hex2int(String hexStr) {
        if (hexStr == null || "".equals(hexStr)) {
            return null;
        }
        hexStr = hexStr.toUpperCase();
        if (hexStr.startsWith("0X")) {
            hexStr = hexStr.substring(2);
        }
        hexStr = hexStr.replace("^0+", "");//去掉头部的0
        if (!hexStr.matches("[0-9a-fA-F]+")) {
            return null;
        }
        int ret = 0;
        int len = hexStr.length();
        for (int i = 0; i < len; i++) {
            ret <<= 4;
            int ch = hexStr.charAt(i);
//            if(ch>64&&ch<71){//A-F
////                ch -= 64;
////                ch += 9;
//                ch -= 55;
//            }else if(ch>47&&ch<58){
//                ch -= 48;
//            }
            ch -= ch>64? 55: 48;
            ret |= ch&15;
        }
        return ret;
    }
    
    
    /**
     * 二进制字符串转数字
     * @param binaryStr
     * @return int数字
     */
    public static Integer toInteger(String binaryStr) {
        if (binaryStr == null
                ||"".equals(binaryStr)
                ||!binaryStr.matches("[01]+")) {
            return null;
        }
        int ret = 0;
        for (int i = 0; i<binaryStr.length(); i++) {
            String c = binaryStr.substring(i, i + 1);
            ret = ret<<1|Integer.parseInt(c);
        }
        return ret;
    }
    /**
     * 取整数m的第n位
     * @param m
     * @param n
     * @return
     */
    public static int getBit(int m, int n) {
        return m>>(n-1)&1;
    }
    
    /**
     * 设置整数m的第n位置为0
     * @param m
     * @param n n>=0
     * @return
     */
    public static int setBitZero(int m, int n) {
        return m & ~(1 << n);
    }
    
    /**
     * 设整数m的第n位置为1
     * @param m
     * @param n n>=0
     * @return
     */
    public static int setBitOne(int m, int n) {
        return m | (1 << n);
    }
    
    /**
     * 翻转整数m的第n位
     * @param m
     * @param n n>=0
     * @return
     */
    public static int setBitReverse(int m, int n) {
        return m ^ (1 << n);
    }
    
    /**
     * 获取二进制中1的个数
     * @param m
     * @return
     */
    public static int getBitOneNum(int m) {
        int count = 0;
        while (m > 0) {
            count++;
            m &= m - 1;
        }
        return count;
    }
    
    /**
     * 获取余数
     * @param numerator
     * @param denominator
     * @return
     */
    public static int getRemainder(int numerator,int denominator ) {
        return numerator < denominator ? numerator : (
                numerator ^ denominator);
    }
    
    /**
     * 不小于此数的最大2的幂次方
     * @param m
     * @return
     */
    public static int toGtIntClose(int m) {
        if (m < 0) {
            return -toLtIntClose(-m);
        }
        return (m & (m - 1)) == 0 ? m :
                (1 << toBinaryStr(m).length());
    }
    
    /**
     * 不大于此数的最大2的幂次方
     * @param m
     * @return
     */
    public static int toLtIntClose(int m) {
        if (m < 0) {
            return -toGtIntClose(-m);
        }
        return (m & (m - 1)) == 0 ? m :
                (1 << toBinaryStr(m).length() - 1);
    }
    
    /**
     * 比较大小
     * @param m
     * @param n
     * @return
     *
    思路（待比较的数为x和y）：
        1. x和y异或，找出所有x和y不同的位，即x^y；
        2. 分别找到x和y在其不同位中的具体值，即：
        a=x&(x^y);
        b=y&(x^y);
        3. 从第一个不同的位后的首个‘1’开始，将右边的位全置1，做法如下：
        a |= a >> 1; a |= a >> 2; a |= a >> 4; a |= a >> 8; a |= a >> 16;
        b |= b >> 1; b |= b >> 2; b |= b >> 4; b |= b >> 8; b |= b >> 16;
        其目的是在下一步中唯一确定x和y中的首个不同的位，但表现在a和b中。
        4. 确定x和y中的首个不同的位，用异或，找出能反映两数大小的信息：
        a=a&(a^b);
        //a大返回正值，a小返回负值，相等为0
     */
    public static int compare(int m, int n) {
        int a = m & (m ^ n);
        int b = n & (m ^ n);
        a |= a >> 1; a |= a >> 2; a |= a >> 4; a |= a >> 8; a |= a >> 16;
        b |= b >> 1; b |= b >> 2; b |= b >> 4; b |= b >> 8; b |= b >> 16;
        return a & (a ^ b);//TODO 返回结果不对
    }
    
    //四则运算
    public static class ASMD {
    
        /**
         * 加法计算器
         * @param m
         * @param n
         * @return
         */
        public static int add(int m, int n) {
            int sum = m^n;          //不进位 加
            int carry = (m&n)<<1;   //只进位 加
            while (carry > 0) {     //进位(carry)不为0，重复上两个步骤
                int a = sum,b = carry;
                sum = a^b;
                carry = (a&b)<<1;
            }
            return sum;
        }
    
        /**
         * 减法计算器
         * @param m
         * @param n
         * @return
         */
        public static int substract(int m, int n) {
            int substractor = add(~n, 1);   //减数的补码表示
            return add(m, substractor);
        }
    
        /**
         * 乘法计算器
         * @param m
         * @param n
         * @return
         */
        public static int multiply(int m, int n) {
            //得到绝对值
            int x = m < 0 ? add(~m, 1) : m;
            int y = n < 0 ? add(~n, 1) : n;
            //使用加法计算乘积
            int ret = 0;
            while (y > 0) {
                //优化写法
                if ((y & 1) == 1) {
                    ret = add(ret, x);
                }
                x <<= 1;
                y >>= 1;
            }
            return (x ^ y) < 0 ? add(~ret, 1) : ret;//确定符号位
        }
    
        /**
         * 除法计算器
         * @param m 除数
         * @param n 被除数
         * @return 商
         */
        public static int divid(int m, int n) {
            return dividRaw(m, n,false);
        }
    
        /**
         * 除法计算器
         * @param m 除数
         * @param n 被除数
         * @param returnReminder 是否返回余数
         * @return  商或余数
         */
        public static int dividRaw(int m, int n,boolean returnReminder) {
            //取绝对值
            int dividend = m > 0 ? m : add(~m, 1);
            int divisor = n > 0 ? n : add(~n,1);
            
            int quotient = 0;//商
            int remainder = 0; //余数
            for (int i=31; i>=0; i--) {
                if ((dividend >> i) >= divisor) {
                    quotient = add(quotient, 1 << i);
                    dividend = substract(dividend, divisor << i);
                }
            }
            //确定商的符号
            if ((m ^ n) < 0) {
                quotient = add(~quotient, 1);
            }
            
            //确定余数的符号
            remainder = n > 0 ? dividend : add(~dividend, 1);
            
            return returnReminder?remainder:quotient;
        }
    }
    
    /**
     * 使hash均匀分布
     * @param hash
     * @return
     */
    public static int evenHash(int hash) {
        int h = hash;
        h ^= (h >>> 20) ^ (h >>> 12);
        return h ^ (h >>> 7) ^ (h >>> 4);
    }
    public static int evenHash(long hash) {
        int h = (int)(hash&0x7FFFFFFF);
        h ^= (h >>> 20) ^ (h >>> 12);
        return h ^ (h >>> 7) ^ (h >>> 4);
    }

   
    
    @Test
    public void test() {
        UtilLogger.println("10 =>"+toBinaryStr(10));
        UtilLogger.println("\"1010\" =>"+toInteger("1010"));
        
        UtilLogger.println("hex: "+toHexStr(452));
        UtilLogger.println("dec: "+hex2int("0x1c4"));
        int i = getRemainder(5, 3);
        UtilLogger.println(i);
    
        UtilLogger.println(toGtIntClose(-5));
    }
    
    @Test
    public void testASMD() {
        UtilLogger.println(compare(3,2));
        UtilLogger.println(compare(2,2));
        UtilLogger.println(compare(2,3));
        
        UtilLogger.println(ASMD.add(1, 2));
        UtilLogger.println(ASMD.substract(1, 2));
        UtilLogger.println(ASMD.multiply(2, 3));
        UtilLogger.println(ASMD.divid(2, 3));
    }
    
    @Test
    public void testHash() {
        int[] hashs = new int[]{
                0xF,0xFF,0xFFF,0xFFFF,0xFFFFF,0xFFFFFF,0xFFFFFFF,
                0x0F000000,0x0FF00000,0x0FFF0000,0x0FFFF000,0x0FFFFF00,0x0FFFFFF0,
                0x0F000000,0x00F00000,0x000F0000,0x0000F000,0x00000F00,0x000000F0
        };
        for (int i = 0; i < hashs.length; i++) {
            UtilLogger.println("hash: "+ UtilBit.toBinaryStr(hashs[i])+
            "\tevenHash: "+ UtilBit.toBinaryStr(evenHash(hashs[i])));
        }
        /**
         result
         hash: 1111	evenHash: 1111
         hash: 11111111	evenHash: 11110001
         hash: 111111111111	evenHash: 111100011111
         hash: 1111111111111111	evenHash: 1111000111110000
         hash: 11111111111111111111	evenHash: 11110001111100001110
         hash: 111111111111111111111111	evenHash: 111100011111000011101111
         hash: 1111111111111111111111111111	evenHash: 1111000111110000111011110001
         hash: 1111000000000000000000000000	evenHash: 1111111011101111111000011110
         hash: 1111111100000000000000000000	evenHash: 1111000100000001000111111111
         hash: 1111111111110000000000000000	evenHash: 1111000111111111111100000001
         hash: 1111111111111111000000000000	evenHash: 1111000111110000000111101110
         hash: 1111111111111111111100000000	evenHash: 1111000111110000111000000000
         hash: 1111111111111111111111110000	evenHash: 1111000111110000111011111110
         hash: 1111000000000000000000000000	evenHash: 1111111011101111111000011110
         hash: 111100000000000000000000	evenHash: 111111101110111111100001
         hash: 11110000000000000000	evenHash: 11111110111011111110
         hash: 1111000000000000	evenHash: 1111111011101111
         hash: 111100000000	evenHash: 111111101110
         hash: 11110000	evenHash: 11111110
         */
    }
    
    
}
