package cn.pheker.utils;

/**
 * @author cn.pheker
 * @date 2017/11/19  11:30
 * @email 1176479642@qq.com
 * @desc    计算耗时
 */
public class UtilElapsedTime<E> {
    
    
    
    public static double test(Callback cb) {
        long startTime=System.nanoTime();
        Object ret = cb.compute();
        double estimatedTime =(double) (System.nanoTime()-startTime)/1000000;
        if (estimatedTime > 1000) {
            StringBuilder sb = new StringBuilder();
            sb.append("["+(int)Math.floor(estimatedTime / 1000) + "s ");
            String[] split = String.valueOf(estimatedTime).split("\\.");
            sb.append(split[0].substring(split[0].length()-3)+ "." + split[1] + "ms] ");
            UtilLogger.print(sb.toString());
        }else {
            UtilLogger.print("[" + estimatedTime + "ms] ");
        }
        UtilLogger.println(ret);
        return estimatedTime ;
    }
    
    public interface  Callback{
        Object compute();
    }
}
