package cn.pheker.utils;

import com.alibaba.fastjson.JSON;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/3/30 14:04
 * email     1176479642@qq.com
 * desc      fastjson工具类
 *
 * </pre>
 */
public class UtilJson {
    
    public static String toJson(Object object){
        return JSON.toJSONString(object);
    }
    public static Object toObject(String json,Class clazz){
        return JSON.parseObject(json, clazz);
    }
}
