package cn.pheker.utils;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/5/30 22:11
 * email     1176479642@qq.com
 * desc      bean map互转工具类
 *
 * </pre>
 */
public class UtilBean {
    
    
    /**
     * bean转map
     * @param bean 等转换的bean
     * @return 转换后的map
     */
    public static <E> Map<String,E> bean2map(Object bean){
        HashMap<String, E> retMap = new HashMap<String,E>();
        if(bean == null) return retMap;
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(bean.getClass());
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor dp : propertyDescriptors) {
                String key = dp.getName();
                if(key.equals("class")) continue;//过滤class属性
                Method getter = dp.getReadMethod();
                E value = (E) getter.invoke(bean);
                retMap.put(key, value);
            }
        } catch (IntrospectionException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return retMap;
    }
    
    /**
     * map转换成bean
     * 注：对应的Bean不能是接口、抽象类，且必须有无参构造方法
     * @param map   要转换的map
     * @param beanClass 将要转换成的Bean的class对象
     * @param <T> 泛型
     * @return 指定的bean
     */
    public static <T,E> T map2bean(Map<String, E> map, Class<T> beanClass) {
        if(map == null || map.size() == 0) return null;
        try {
            T bean = beanClass.newInstance();
            BeanInfo beanInfo = Introspector.getBeanInfo(beanClass);
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor property: propertyDescriptors) {
                String key = property.getName();
                if (!key.equals("class")) {
                    Method setter = property.getWriteMethod();
                    setter.invoke(bean, map.get(key));
                }
            }
            return bean;
        } catch (IntrospectionException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            //java.lang.InstantiationException
            //bean为接口、抽象类或没有无参构造方法时报错
            e.printStackTrace();
            UtilLogger.println("注：对应的Bean不能是接口、抽象类，且必须有无参构造方法");
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * 将List中的map全部转换成bean
     * @param listMap   元素类型为map的List
     * @param beanClass 要转换成的bean的class对象
     * @param <T>   泛型
     * @return 元素类型为bean的List
     */
    public static <E,T> List<T> map2beanOfList(List<Map<String, E>> listMap, Class<T> beanClass) {
        int size = listMap.size();
        List<T> retList = new ArrayList<T>(size);
        for (int i = 0; i < size; i++) {
            Map<String, E> map = listMap.get(i);
            T t = map2bean(map, beanClass);
            retList.add(t);
        }
        return retList;
    }
    
    /**
     * 将List中的bean全部转换成map
     * @param listBean  元素类型为bean的List
     * @param <T> 泛型
     * @return 元素类型为map的List
     */
    public static <T,E> List<Map<String,E>> bean2mapOfList(List<T> listBean) {
        int size = listBean.size();
        List<Map<String,E>> retList = new ArrayList<Map<String,E>>(size);
        for (int i = 0; i < size; i++) {
            T t = listBean.get(i);
            Map<String,E> map = bean2map(t);
            retList.add(map);
        }
        return retList;
    }
    
    /**
     * 批量插入元素到指定的list
     * 如果list为null,会自动new一个list
     * @param list 要插入的list容器,如果list为null,会自动new一个list
     * @param objs 要插入的元素
     * @param <T>  泛型
     * @return     list
     */
    public static <T> List<T> batchInsert(List<T> list, T... objs){
        if(objs == null) return list;
        if (list == null) {
            list = new ArrayList<T>();
        }
        
        for (T obj : objs) {
            list.add(obj);
        }
        return list;
    }
    
    public static void main(String[] args) {
    
        User user = new User("cn.pheker", 18);
        Map<String, Object> map = bean2map(user);
        UtilLogger.printSplitLine("bean2map", map);
    
        User bean = map2bean(map, User.class);
        UtilLogger.printSplitLine("map2bean", bean);
        
        List<User> userList = new ArrayList<User>(){
            {
                add(user);
                add(user);
                add(user);
            }
        };
        List<Map<String, Object>> users = bean2mapOfList(userList);//隐式泛型
        UtilLogger.printSplitLine("bean2mapOfList", users);
    
        List<Map<String,Object>> listMap = new ArrayList<Map<String,Object>>(){
            {
                add(map);
                add(map);
                add(map);
            }
        };
        List<User> users2 = UtilBean.<Object,User>map2beanOfList(listMap, User.class);//显式泛型
        UtilLogger.printSplitLine("map2beanOfList", users2);
        
        
    }
    static class User{
        private String name;
        private int age;
    
        public User() {
        }
    
        public User(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }


        public void setAge(int age) {
            this.age = age;
        }
        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }

}
