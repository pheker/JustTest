package cn.pheker.utils;

import java.util.*;

/**
 * @author  cn.pheker
 * @date    2017/11/27  20:25
 * @email   1176479642@qq.com
 * @desc    ArrayList,HashMap组合工具类,同时具有两者特性
 *          但key只能为String类型
 */
public class ListHashMap<E> {
    
    ArrayList<E> list;
    HashMap<String, E> map;
    
    ArrayList<String> keyList = new ArrayList<>();
    
    
    int defaultCapacity = 16;
    
    public ListHashMap() {
        init(16);
    }
    
    public ListHashMap(int initialCapacity) {
    
    }
    
    public ListHashMap(int initialCapacityList, int initialCapacityMap) {
        list = new ArrayList<>(initialCapacityList);
        map = new HashMap<String,E>(initialCapacityMap);
    }
    
    private void init(int initialCapacity) {
        if (initialCapacity < 2) {
            throw new RuntimeException("initialCapacity 2 at least");
        }
        list = new ArrayList<E>((initialCapacity & 1) == 1 ? initialCapacity / 2 + 1 : initialCapacity / 2);
        map = new HashMap<String,E>(initialCapacity / 2);
    }
    
    
    public int size() {
        return sizeList() + keyList.size();
    }
    
    
    public int sizeList() {
        return list.size();
    }
    
    
    public int sizeMap() {
        return keyList.size();
    }
    
    
    public E get(int index) {
        if (index < 0 || index > size() - 1) {
            throw new RuntimeException("ArrayOutOfBoundsException "+size()+":"+index);
        }
        return index < sizeList() ?
                list.get(index) :
                map.get(keyList.get(index - sizeList()));
    }
    
    
    public E get(String key) {
        return map.get(key);
    }
    
    
    public E add(int index, E e) {
        return add(sizeList(), e);
    }
    
    
    public E add(E e) {
        boolean addSuccess = list.add(e);
        return addSuccess ? e : null;
    }
    
    
    public E add(String key, E e) {
        keyList.add(key);
        return map.put(key, e);
    }
    
    
    public void addAll(Collection<? extends E> es) {
        list.addAll(es);
    }
    
    
    public void addAll(Map<? extends String,? extends E> m) {
        map.putAll(m);
    }
    
    
    public E remove(int index) {
        if (index < size()) {
            return list.remove(index);
        }else{
            String removedKey = keyList.remove(index - sizeList() - 1);
            return map.remove(removedKey);
        }
    }
    
    
    public E removeByKey(String key) {
        keyList.remove(key);
        return map.remove(key);
    }
    
    
    public Set<String> keySet() {
        return map.keySet();
    }
    
    
    public int remove(E e) {
        int removeCount = 0;
        if (list.remove(e)) {
            removeCount++;
        }
        if (map.containsValue(e)) {
            Set<String> keySet = map.keySet();
            for (String key :keySet) {
                if(map.get(key).equals(e)){
                    keyList.remove(key);
                    map.remove(key);
                    removeCount++;
                }
            }
        }
        return removeCount;
    }
    
    
    public void clear() {
        list.clear();
        map.clear();
        keyList.clear();
    }
    
    
    public boolean contains(String key) {
        return map.containsKey(key);
    }
    
    
    public boolean contains(E e) {
        return list.contains(e) ? true : map.containsValue(e);
    }
    
    
    public ListMapIterator<E> iterator() {
        return new ListMapIterator<E>();
    }
    
    public class ListMapIterator<E> implements Iterator<E> {
    
        int cursor = 0;
        boolean nextInvoked = false;
        Iterator<E> listIter = (Iterator<E>) list.iterator();
        Iterator<String> mapIter = map.keySet().iterator();
        
        
        public boolean hasNext() {
            nextInvoked = false;
            return cursor < size();
        }
        
        
        public E next() {
            cursor++;
            nextInvoked = true;
            return cursor <= sizeList() ? listIter.next() : (E) map.get(mapIter.next());
        }
        
        
        public void remove() {
            if (cursor < sizeList()) {
                listIter.remove();
            }else{
                mapIter.remove();
                keyList.remove(cursor - sizeList()-1);
            }
            cursor--;
        }
        
        public Detail detail() {
            int index = nextInvoked? cursor-1:cursor;
            String key = index < sizeList() ? "" : keyList.get(index - sizeList());
            return new Detail(index, key, get(index));
        }
    }
    
    
    public int hashCode() {
        int h = 0;
        ListMapIterator<E> i = this.iterator();
        while (i.hasNext())
            h += i.next().hashCode();
        return h;
    }
    
    
    
    public boolean equals(Object obj) {
        if (obj instanceof ListHashMap) {
            ListHashMap target = (ListHashMap) obj;
            return target.map.equals(this.map)
                    && target.list.equals(this.list);
        }
        return false;
    }
    
    public class Detail {
        int index;
        String key;
        E value;
    
        public Detail(int index, E value) {
            this.index = index;
            this.key = "";
            this.value = value;
        }
        public Detail(int index, String key, E value) {
            this.index = index;
            this.key = key;
            this.value = value;
        }
    
        public int getIndex() {
            return index;
        }
    
        public void setIndex(int index) {
            this.index = index;
        }
    
        public String getKey() {
            return key;
        }
    
        public void setKey(String key) {
            this.key = key;
        }
    
        public E getValue() {
            return value;
        }
    
        public void setValue(E value) {
            this.value = value;
        }
    
        
        public String toString() {
            return "{" +
                    "index:" + index +
                    ", key:\"" + key + '\"' +
                    ", value:" + (value instanceof String ? "\"" + value + "\"" : value) +
                    '}';
        }
        
    }
    
    
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        int size = size();
        int listSize = list.size();
        for (int i = 0; i < size; i++) {
            E e = get(i);
            Detail po = i < listSize ? new Detail(i, e) :
                    new Detail(i, keyList.get(size - i - 1), e);
            sb.append(po.toString()+",");
        }
        if (sb.length() > 1) {
            sb.replace(sb.length()-1,sb.length(),"]");
        }else {
            sb.append("]");
        }
        return sb.toString();
    }
    
    public String toStringSimple() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        int size = size();
        for (int i = 0; i < size; i++) {
            E e = get(i);
            sb.append((e instanceof String ? "\"" + e + "\"":e)+",");
        }
        if (sb.length() > 1) {
            sb.replace(sb.length()-1,sb.length(),"]");
        }else {
            sb.append("]");
        }
        return sb.toString();
    }
}
