package cn.pheker.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

import static cn.pheker.utils.Clzz.ClzzBuilder.Tags.*;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/3/23 13:52
 * email     1176479642@qq.com
 * desc      解析class文件，读取内容
 *
 * </pre>
 */
public class UtilClass {
    public static void main(String[] args) {
        Clzz clzz = UtilClass.build("C:\\Users\\cnpheker\\Desktop\\ConsistentHashing.class");
        UtilLogger.println(clzz);
//        UtilLogger.println(clzz.getConstantPool());
    }
    
    private static File fileClzz;
    private static byte[] bs;
    private UtilClass() {
    }
    
    private UtilClass(String clzzPath) {
        this.fileClzz = new File(clzzPath);
        try {
            FileInputStream is = new FileInputStream(fileClzz);
            bs = new byte[is.available()];
            is.read(bs);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static Clzz build(String clzzPath) {
        UtilClass utilClass = new UtilClass(clzzPath);
        int magic = toInt(0, 4);
        int minor = toInt(4,2);
        int major = toInt(6,2);
        
        int poolsOffset = 4+2+2;
        int poolsCount = toInt(poolsOffset,2);
        


//
//        int methodOffset = fieldOffset+2+fieldCount;
//        int methodCount = toInt(methodOffset, 2);
//
//        int attributesOffset = methodOffset+2+methodCount;
//        int attributesCount = toInt(attributesOffset, 2);
        Clzz.ClzzBuilder builder = Clzz.ClzzBuilder.builder()
                .isClzz(magic == 0xCAFEBABE)
                .minorVersion(minor)
                .majorVersion(major)
                .constantPoolCount(poolsOffset,poolsCount);
    
        int accessFlagsOffset = builder.offset;
        int interfacesOffset = accessFlagsOffset+6;
        int interfacesCount = toInt(interfacesOffset,2);

        builder.accessFlags(toInt(accessFlagsOffset, 2) + "")
                .thisClass(toInt(accessFlagsOffset+2, 2) + "")
                .superClass(toInt(accessFlagsOffset+4, 2) + "")
                .interfacesCount(interfacesOffset,interfacesCount);


        int fieldOffset = builder.offset;
        int fieldCount = toInt(fieldOffset,2);
        builder.fieldsCount(fieldOffset,fieldCount);
//                .methodsCount(methodCount)
//                .attributesCount(attributesCount)
        
        return builder.build();
    }
    
    public static int toInt(int offset, int len) {
        return toInt(bs, offset, len);
    }
    /**
     * 从offset的后面一位开始转换
     * @param bs        字节数组
     * @param offset    偏移
     * @param len       要截取的长度
     * @return
     */
    public static int toInt(byte[] bs, int offset, int len) {
        if(bs.length==0||bs.length-offset-len+1<0){
            throw new RuntimeException("out of byte array index");
        }else if(len < 1||len>4){
            throw new RuntimeException("len must between 1 and 4");
        }
        int value = 0x0;
        int i = 0;
        while(len-- > 0){
            value |= (bs[offset + (i++)] & 0xff) << (8 * len);
        }
        return value;
    }
    
    public static byte[] slice(int offset, int len) {
        byte[] copy = new byte[len];
        System.arraycopy(bs, offset, copy, 0, len);
        return copy;
    }
    

}

class Clzz {
    boolean isClzz = false;
    int minorVersion = 0;
    int majorVersion = 0;
    //pool
    int constantPoolCount = 0;
    ArrayList<ClzzBuilder.Constant> constantPool;
    String accessFlags;
    String thisClass;
    String superClass;
    
    int interfacesCount = 0;
    //interfaces
    
    int fieldsCount = 0;
    //fields
    
    int methodsCount = 0;
    //methods
    
    int attributesCount = 0;
    
    public boolean isClzz() {
        return isClzz;
    }
    
    public int getMinorVersion() {
        return minorVersion;
    }
    
    public int getMajorVersion() {
        return majorVersion;
    }
    
    public int getConstantPoolCount() {
        return constantPoolCount;
    }
    
    public ArrayList<ClzzBuilder.Constant> getConstantPool() {
        return constantPool;
    }
    
    public String getAccessFlags() {
        return accessFlags;
    }
    
    public String getThisClass() {
        return thisClass;
    }
    
    public String getSuperClass() {
        return superClass;
    }
    
    public int getInterfacesCount() {
        return interfacesCount;
    }
    
    public int getFieldsCount() {
        return fieldsCount;
    }
    
    public int getMethodsCount() {
        return methodsCount;
    }
    
    public int getAttributesCount() {
        return attributesCount;
    }
    
    @Override
    public String toString() {
        return "Clzz{" +
                "isClzz=" + isClzz +
                ", minorVersion=" + minorVersion +
                ", majorVersion=" + majorVersion +
                ", constantPoolCount=" + constantPoolCount +
                ", accessFlags='" + accessFlags + '\'' +
                ", thisClass='" + thisClass + '\'' +
                ", superClass='" + superClass + '\'' +
                ", interfacesCount=" + interfacesCount +
                ", fieldsCount=" + fieldsCount +
                ", methodsCount=" + methodsCount +
                ", attributesCount=" + attributesCount +
                '}';
    }
    
    public static final class ClzzBuilder {
        boolean isClzz = false;
        int minorVersion = 0;
        int majorVersion = 0;
        
        int constantPoolCount = 0;
        ArrayList<Constant> constantPool;
        
        String accessFlags;
        String thisClass;
        String superClass;
        
        int interfacesCount = 0;
        private ArrayList<String> interfaces;
        
        int fieldsCount = 0;
        int methodsCount = 0;
        int attributesCount = 0;
        
        int offset = 0;
    
        private ClzzBuilder() {
        }
        
        public static ClzzBuilder builder() {
            return new ClzzBuilder();
        }
        
        public ClzzBuilder isClzz(boolean isClzz) {
            this.isClzz = isClzz;
            return this;
        }
        
        public ClzzBuilder minorVersion(int minorVersion) {
            this.minorVersion = minorVersion;
            return this;
        }
        
        public ClzzBuilder majorVersion(int majorVersion) {
            this.majorVersion = majorVersion;
            return this;
        }
        
        public ClzzBuilder constantPoolCount(int offset,int constantPoolCount) {
            offset += 2;
            this.constantPoolCount = constantPoolCount;
            this.constantPool = new ArrayList<Constant>(310);
            int[] offsets = new int[constantPoolCount];
            
            //添加常量
            int count = 0, incr = 0;
            while (count < constantPoolCount-1) {//0-309 = 310个， 311-1
                int tag = UtilClass.toInt(offset+incr, 1);
                incr++;
                Constant constant = null;
                switch (tag) {
                    //7-8 15
                    case CONSTANT_Class:
                    case CONSTANT_String:
                    case CONSTANT_MethodType:
                        constant = new Constant(tag,UtilClass.toInt(offset + incr, 2),2);
                        incr += 2;
                        break;
                        
                    //1
                    case CONSTANT_Utf8:
                        int len = UtilClass.toInt(offset + incr, 2);
                        incr += 2;
                        constant = new Constant(tag,len+2,UtilClass.slice(offset+incr,len));
                        incr += len;
                        break;
                        
                    //3-4
                    case CONSTANT_Integer:
                    case CONSTANT_Float:
                        constant = new Constant(tag, UtilClass.toInt(offset + incr, 4),4);
                        incr += 4;
                        break;
                        
                    //5-6
                    case CONSTANT_Long:
                    case CONSTANT_Double:
                        constant = new Constant(tag,8, UtilClass.slice(offset+incr,8));
                        incr += 8;
                        break;
                        
                    //9-12 18
                    // two index 2 + 2
                    case CONSTANT_Fieldref:
                    case CONSTANT_Methodref:
                    case CONSTANT_InterfaceMethodref:
                    case CONSTANT_NameAndType:
                    case CONSTANT_MethodInvokeDynamic:
                        constant = new Constant(tag,4, UtilClass.slice(offset+incr,4));
                        incr += 4;
                        break;
                        
                    //15
                     case CONSTANT_MethodHandle:
                        constant = new Constant(tag,3, UtilClass.slice(offset+incr,3));
                        incr += 3;
                        break;
                        
                        
                }
                offsets[count+1] = incr;
                constantPool.add(count++,constant);
//                UtilLogger.println(count,incr,constant);
                //8字节的 占两个常量池项
                if (tag == CONSTANT_Long || tag == CONSTANT_Double) {
                    constantPool.add(count++,new Constant(0, 0));
                }
            }
            this.offset = offset+incr;
            return this;
        }
        
        public ClzzBuilder accessFlags(String accessFlags) {
            this.accessFlags = accessFlags;
            return this;
        }
        
        public ClzzBuilder thisClass(String thisClass) {
            this.thisClass = thisClass;
            return this;
        }
        
        public ClzzBuilder superClass(String superClass) {
            this.superClass = superClass;
            return this;
        }
        
        public ClzzBuilder interfacesCount(int offset, int interfacesCount) {
            this.interfacesCount = interfacesCount;
            this.interfaces = new ArrayList<String>();
            //todo deal interfaces
            offset += 2;
            int i = 0, incr = 0;
            while (i++ < interfacesCount) {
            
            }
            this.offset = offset+incr;
            return this;
        }
        
        public ClzzBuilder fieldsCount(int offset, int fieldsCount) {
            this.fieldsCount = fieldsCount;
            offset += 2;
            int i = 0, incr = 0;
            while (i++ < fieldsCount) {
            
            }
            
            this.offset = offset + incr;
            return this;
        }
        
        public ClzzBuilder methodsCount(int methodsCount) {
            this.methodsCount = methodsCount;
            return this;
        }
        
        public ClzzBuilder attributesCount(int attributesCount) {
            this.attributesCount = attributesCount;
            return this;
        }
        
        public Clzz build() {
            Clzz clzz = new Clzz();
            clzz.minorVersion = this.minorVersion;
            clzz.constantPoolCount = this.constantPoolCount;
            clzz.constantPool = this.constantPool;
            clzz.thisClass = this.thisClass;
            clzz.accessFlags = this.accessFlags;
            clzz.methodsCount = this.methodsCount;
            clzz.interfacesCount = this.interfacesCount;
            clzz.isClzz = this.isClzz;
            clzz.superClass = this.superClass;
            clzz.majorVersion = this.majorVersion;
            clzz.fieldsCount = this.fieldsCount;
            clzz.attributesCount = this.attributesCount;
            return clzz;
        }
        
        public static class Tags{
            public static final int CONSTANT_Utf8       = 1;
            public static final int CONSTANT_Integer    = 3;
            public static final int CONSTANT_Float	    = 4;
            public static final int CONSTANT_Long	    = 5;
            public static final int CONSTANT_Double	    = 6;
            public static final int CONSTANT_Class	    = 7;
            public static final int CONSTANT_String	    = 8;
            public static final int CONSTANT_Fieldref	= 9;
            public static final int CONSTANT_Methodref	= 10;
            public static final int CONSTANT_InterfaceMethodref	    = 11;
            public static final int CONSTANT_NameAndType	        = 12;
            public static final int CONSTANT_MethodHandle	        = 15;
            public static final int CONSTANT_MethodType	            = 16;
            public static final int CONSTANT_MethodInvokeDynamic	= 18;
        }
        
        public static class Constant{
            int tag;
            int index;
            int length;
            byte[] bytes;
    
            public Constant(int tag, int index, int length, byte[] bytes) {
                this.tag = tag;
                this.index = index;
                this.length = length;
                this.bytes = bytes;
            }
            public Constant(int tag, int index, int length) {
                this.tag = tag;
                this.index = index;
                this.length = length;
                this.bytes = new byte[0];
            }
            public Constant(int tag, int length, byte[] bytes) {
                this.tag = tag;
                this.index = index;
                this.length = length;
                this.bytes = bytes;
            }
            public Constant(int tag, int index) {
                this.tag = tag;
                this.index = index;
                this.length = 0;
                this.bytes = new byte[0];
            }
    
            public int getTag() {
                return tag;
            }
    
            public void setTag(int tag) {
                this.tag = tag;
            }
    
            public int getIndex() {
                return index;
            }
    
            public void setIndex(int index) {
                this.index = index;
            }
    
            public int getLength() {
                return length;
            }
    
            public void setLength(int length) {
                this.length = length;
            }
    
            public byte[] getBytes() {
                return bytes;
            }
    
            public void setBytes(byte[] bytes) {
                this.bytes = bytes;
            }
    
            @Override
            public String toString() {
                return "Constant{" +
                        "tag=" + tag +
                        ", index=" + index +
                        ", length=" + length +
                        ", bytes=" + Arrays.toString(bytes) +
                        '}';
            }
        }
    }
    //attributes
    
    
}
