package cn.pheker.utils;

import org.junit.Test;

/**
 * @author cn.pheker
 * @date 2017/11/25  18:28
 * @email 1176479642@qq.com
 * @desc
 */
public class UtilString {
    
    public static String repeat(String ch, int time) {
        StringBuilder sb = new StringBuilder();
        while (time-- > 0) {
            sb.append(ch);
        }
        return sb.toString();
    }
    
    @Test
    public void test() {
        UtilLogger.println(repeat("0",2));
    }
    
    /**
     * 格式化输出,类似String.format,仅使用参数替换对应的{}
     * @param fmtString 格式化字符串
     * @param params    对应参数
     * @return          格式化后的字符串
     */
    public static String format(String fmtString, Object... params) {
        int len = params.length;
        String[] split = fmtString.split("\\{\\}");
        boolean end = fmtString.endsWith("{}");
        int fmtNum = split.length - (end?0:1);
        if ( fmtNum != len)
            throw new RuntimeException("format number["+fmtNum+"] does not equals params length["+len+"]");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < len; i++) {
            sb.append(split[i]);
            sb.append(String.valueOf(params[i]));
        }
        if(!end) sb.append(split[split.length - 1]);
        return sb.toString();
    }
    
    @Test
    public void testFormat() {
        String ret = format("{name:{},price:{},author:cn.pheker}", "book",55.0);
        UtilLogger.printSplitLine("format",ret);
    }
    
}
