package cn.pheker.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * @author cn.pheker
 * @date 2017/11/19  11:09
 * @email 1176479642@qq.com
 * @desc
 */
public class UtilLogger {
    
    public synchronized static void print(Object obj) {
        System.out.print(obj);
    }
    private synchronized static void println(Object obj) {
        System.out.println(obj);
    }
    public synchronized static void println(Object... objs) {
        int size = objs.length;
        int i = 0;
        for(;i<size;i++) {
            if (i < size - 1) {
                print(objs[i]+",");
            }else{
                println(objs[i]);
            }
        }
    }
    public synchronized static void echoList(List list) {
        System.out.println(Arrays.toString(list.toArray()));
    }
    public synchronized static void echoArray(Object[] arr) {
        System.out.println(Arrays.toString(arr));
    }
    public synchronized static void echoSet(Set<? extends Object> set) {
        System.out.println(Arrays.toString(set.toArray()));
    }
    
    public synchronized static void printSplitLine(String lineInfo) {
        println(String.format("-------------------- %s --------------------",lineInfo));
    }
    public synchronized static void  printSplitLine(String lineInfo,Object... obj) {
        printSplitLine(lineInfo);
        println(obj);
    }
}
