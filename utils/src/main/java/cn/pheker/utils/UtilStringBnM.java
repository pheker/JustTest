package cn.pheker.utils;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/3/20 15:36
 * email     1176479642@qq.com
 * desc      Boyer-Moore算法查找
 * 链接
 * 阮一锋     http://www.ruanyifeng.com/blog/2013/05/boyer-moore_string_search_algorithm.html
 * 维基百科   https://en.wikipedia.org/wiki/Boyer%E2%80%93Moore_string_search_algorithm
 *
 * </pre>
 */
public class UtilStringBnM {
    
    public static void main(String[] args) {
        String text = "hello000000000000888safdslkn;eewmmfdf;dfgp;'" +
                "''/fg/;sdkf,klsdfd',sdf;sdf;,df156489nxncvsdlnmdfdgdfgdfgdg34353454345gfhgfhgf" +
                "sdfsfsdfdcxv8888888888888888888888888800000000000,jack1111111111";
        UtilLogger.println(indexOf(text,"ab"));
        UtilElapsedTime.test(() -> {
            int i = indexOf(text, "ack1111111111");
            return "BnM indexOf: "+i;
        });
        UtilElapsedTime.test(() -> {
            int i = indexOf2(text.toCharArray(), "ack1111111111".toCharArray());
            return "BnM indexOf2: "+i;
        });
        UtilElapsedTime.test(() -> {
            int i = text.indexOf("ack1111111111");
            return "jdk indexOf: "+i;
        });
        
    }
    
    /**
     * 返回指定的字符串target在字符串text中第一次出现的位置,如果不存在返回-1
     * @param text      文本字符串
     * @param target    要搜索的字符串
     * @return          首次出现的位置
     */
    public static int indexOf(String text, String target) {
        return indexOf2(text.toCharArray(), target.toCharArray());
    }
    
    /**
     * Returns the index within this string of the first occurrence of the
     * specified substring. If it is not a substring, return -1.
     *
     * @param haystack The string to be scanned
     * @param needle The target string to search
     * @return The start index of the substring
     */
    public static int indexOf2(char[] haystack, char[] needle) {
        if (needle.length == 0) {
            return 0;
        }
        int charTable[] = makeCharTable(needle);
        int offsetTable[] = makeOffsetTable(needle);
        for (int i = needle.length - 1, j; i < haystack.length;) {
            for (j = needle.length - 1; needle[j] == haystack[i]; --i, --j) {
                if (j == 0) {
                    return i;
                }
            }
            // i += needle.length - j; // For naive method
            i += Math.max(offsetTable[needle.length - 1 - j], charTable[haystack[i]]);
        }
        return -1;
    }
    
    /**
     * Makes the jump table based on the mismatched character information.
     */
    private static int[] makeCharTable(char[] needle) {
        final int ALPHABET_SIZE = Character.MAX_VALUE + 1; // 65536
        int[] table = new int[ALPHABET_SIZE];
        for (int i = 0; i < table.length; ++i) {
            table[i] = needle.length;
        }
        for (int i = 0; i < needle.length - 1; ++i) {
            table[needle[i]] = needle.length - 1 - i;
        }
        return table;
    }
    
    /**
     * Makes the jump table based on the scan offset which mismatch occurs.
     */
    private static int[] makeOffsetTable(char[] needle) {
        int[] table = new int[needle.length];
        int lastPrefixPosition = needle.length;
        for (int i = needle.length; i > 0; --i) {
            if (isPrefix(needle, i)) {
                lastPrefixPosition = i;
            }
            table[needle.length - i] = lastPrefixPosition - i + needle.length;
        }
        for (int i = 0; i < needle.length - 1; ++i) {
            int slen = suffixLength(needle, i);
            table[slen] = needle.length - 1 - i + slen;
        }
        return table;
    }
    
    /**
     * Is needle[p:end] a prefix of needle?
     */
    private static boolean isPrefix(char[] needle, int p) {
        for (int i = p, j = 0; i < needle.length; ++i, ++j) {
            if (needle[i] != needle[j]) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Returns the maximum length of the substring ends at p and is a suffix.
     */
    private static int suffixLength(char[] needle, int p) {
        int len = 0;
        for (int i = p, j = needle.length - 1;
             i >= 0 && needle[i] == needle[j]; --i, --j) {
            len += 1;
        }
        return len;
    }
}
