package cn.pheker.utils;

/**
 * @author cn.pheker
 * @date 2017/12/15  17:19
 * @email 1176479642@qq.com
 * @desc
 */
public class UtilResult {
    
    public static enum TYPE{
        UNKONW_ERROR(-1, "未知错误"),
        FAIL(0, "失败"),
        SUCCESS(1,"成功"),
        NO_PERMISSION(401, "无权访问"),
        LOGIN_TIME_OUT(402, "登陆状态失效");
        
        int code;
        String msg;
        
        public int getCode() {
            return code;
        }
        
        
        public String getMsg() {
            return msg;
        }
        
        TYPE(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }
    }
    
    public static final TYPE FAIL = TYPE.FAIL;
    public static final TYPE SUCCESS = TYPE.SUCCESS;
    
    
    public static Result success(Object data) {
        return new Result(0,"成功",data);
    }
    
    public static Result fail(int code,String msg){
        return new Result(code, msg, "");
    }
    
    public static Result fail(TYPE fail){
        return fail(fail.code,fail.msg);
    }
    
    public static Result fail(){
        return fail(FAIL);
    }
    
}

