package cn.pheker.annotation;

import java.lang.annotation.*;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/5/1 12:26
 * email     1176479642@qq.com
 * desc     打印
 *
 * </pre>
 */

@Retention(RetentionPolicy.RUNTIME)
@Documented
@Target({ElementType.FIELD,ElementType.LOCAL_VARIABLE,ElementType.METHOD})
public @interface Print {
}
