import cn.pheker.annotation.Print;
import cn.pheker.utils.UtilElapsedTime;
import cn.pheker.utils.UtilLogger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/5/1 12:41
 * email     1176479642@qq.com
 * desc     测试Print注解
 *
 * </pre>
 */
public class TestPrint {

    @Print
    private static String value = "cn.pheker";
    private static int sn = 0;

    @Print
    public static String calc() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return "calc";
    }

    public static void main(String[] args) {
        Field[] fields = TestPrint.class.getDeclaredFields();
        for (Field field : fields) {
            Print print = field.getAnnotation(Print.class);
            if (print != null) {
                UtilLogger.println(print);
                try {
                    Object obj = field.get(TestPrint.class);
                    UtilLogger.println(obj);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

        Method[] ms = TestPrint.class.getDeclaredMethods();
        for (Method m : ms) {
//            UtilLogger.printSplitLine("contain Print",m.isAnnotationPresent(Print.class));

            Print print = m.getAnnotation(Print.class);
            if (print == null) continue;
                UtilElapsedTime.test(()-> {
                    try {
                        m.invoke(TestPrint.class, m.getParameters());
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                    return "耗时 "+m.getName();
                });
        }

    }



}
