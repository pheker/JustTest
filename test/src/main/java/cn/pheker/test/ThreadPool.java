package cn.pheker.test;

import cn.pheker.utils.Range;

import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/4/16 15:31
 * email     1176479642@qq.com
 * desc
 *
 * </pre>
 */
public class ThreadPool {

    public static void main(String[] args) {
        test();
    }

    private static void test() {
        ExecutorService pool = Executors.newCachedThreadPool();
        pool.submit(()->Range.newRange(3).forEach(System.out::println));
        pool.shutdown();
        TreeMap<Object, Object> map = new TreeMap<>();
        ThreadLocal<Map> tl = new ThreadLocal<>();
        tl.set(map);

    }

}
