package cn.pheker.test;

import cn.pheker.utils.Range;
import cn.pheker.utils.UtilElapsedTime;
import cn.pheker.utils.UtilLogger;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/4/23 16:56
 * email     1176479642@qq.com
 * desc      筛选出好数: 数字翻转180度后不等于原来的数
 *           翻转规则如：2<=>5,6<=>9;0,1,8翻转后等于自身;3,4,7不可以翻转
 *
 * </pre>
 */
public class GoodNumber {

    public static void main(String[] args) {
        UtilElapsedTime.test(() -> test());
    }

    private static String test() {
        Stream<Long> list = Range.newRange(100);

        UtilLogger.printSplitLine("num",Range.newRange(100).filter(x-> reverseOfGoodNumber(x) != -1).count());
        //过滤,只保留好数
        List<Long> ret = list.filter(x-> reverseOfGoodNumber(x) != -1)
                .collect(Collectors.toList());
        UtilLogger.printSplitLine("target",ret);

        //打印反转后的结果
        List<Long> reverse = Stream.of(ret).flatMap(x -> x.stream())
                .map(i -> reverseOfGoodNumber(i))
                .collect(Collectors.toList());
        UtilLogger.printSplitLine("reverse",reverse);
        return "GoodNumber";
    }


    /**
     * 反转,替换
     * 如果是好数,返回反转后的结果
     * 如果不是好数,返回-1
     */
    public static Long reverseOfGoodNumber(Long gd) {
        String str = String.valueOf(gd);
        StringBuilder sb = new StringBuilder();

        boolean isGoodNumber = true;
        int len = str.length(),i = len ;
        while (--i > -1 && isGoodNumber) {
            char ch = str.charAt(i);
            switch (ch) {
                case '2':
                    sb.append('5');
                    break;
                case '5':
                    sb.append('2');
                    break;
                case '6':
                    sb.append('9');
                    break;
                case '9':
                    sb.append('6');
                    break;
                case '3':
                case '4':
                case '7':
                    isGoodNumber = false;
                    break;
                default:
                    sb.append(ch);
                    break;
            }
        }

        String reverse = sb.toString();
        //判断不等时 保留
        if(reverse.startsWith("0")||str.equals(reverse)){
            isGoodNumber = false;
        }
        return isGoodNumber? Long.parseLong(reverse):-1L;
    }
}
