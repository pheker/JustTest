package cn.pheker.test;

import cn.pheker.utils.Range;
import cn.pheker.utils.UtilLogger;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/4/27 14:28
 * email     1176479642@qq.com
 * desc     每个用户可以根据自己拥有的金币添加一定数量的矿工(需要使用金币购买)，
 *          矿工的数量和类型决定了挖金币的速率，
 *          挖出的金币达到指定数量(100)会自动添加到自己的总金币数上,
 *          总金币数达到一定数量又可以添加矿工
 *
 * </pre>
 */
public class GoldenMiner {

    private static AtomicLong sn_curr = new AtomicLong(0L);                       //当前序列号
    private static volatile ConcurrentHashMap<Long, User> users = new ConcurrentHashMap<>();//保存所有用户
    private static volatile ArrayList<Miner> miners = Miner.initMiners();                   //保存所用类型的矿工
    private static ExecutorService pool = Executors.newCachedThreadPool();                  //矿工挖矿线程池

    private static final int GOLD_SHOW_INTERVAL = 2000;
    private static final int USER_NUMBER = 5;

    public static void main(String[] args) {
        produce();
    }

    // 创建用户，雇佣矿工，并显示用户的金币
    private static void produce() {
        Range.newRange(USER_NUMBER).forEach(x ->{
            User user = User.newUser();
            UtilLogger.printSplitLine(x+"",user);
            pool.submit(new MinerProduceGoldenThread(user));
        });

        hireMiner();

        pool.submit(new ShowUserGoldThread());
    }

    // 线程:用于显示用户金币
    static class ShowUserGoldThread implements Runnable{
        private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        @Override
        public void run() {
            while(true) {
                UtilLogger.printSplitLine("print "+ sdf.format(Calendar.getInstance().getTime()));
                users.forEach((i, u) -> UtilLogger.println(u));
                try {
                    Thread.sleep(GOLD_SHOW_INTERVAL);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // 雇佣矿工
    private static void hireMiner(){
        long curr = 0;
        while(true){
            long size = users.size();
            if(size == 0){
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                continue;
            }
            if(curr >= size) break;
            long willAdd = curr+1;
            UtilLogger.printSplitLine("sn "+curr);
            int i = 1;
            while(willAdd-- > 0) {
                User user = users.get(curr);
                Miner.MinerInfo minerInfo = user.newMiner();
                int code = minerInfo.getCode();
                if (code != 0) {// 雇佣矿工失败
                    if(code != 1) {
                        willAdd++;
                    }else{
                        UtilLogger.println(i,"fail: ",minerInfo.getMsg(),user.getMoneySum()+"/"+miners.get(i-1).getGolden());
                    }
                }else{
                    UtilLogger.println(i);
                }
                i++;
            }
            curr++;
        }

        // 计算回收周期
        UtilLogger.printSplitLine("计算回收周期");
        users.forEach((i,u) ->{
            ArrayList<Miner> miners = u.getUMiners();
            long speed = u.getMoneyDepositSpeed();
            long sum = 0;
            sum = miners.stream().map(x->x.getGolden()).reduce(sum, (a, b) -> a + b);
            BigDecimal days = new BigDecimal(sum).setScale(4)
                    .divide(new BigDecimal(speed),BigDecimal.ROUND_HALF_UP);
            UtilLogger.println(u.getSn(),sum,days.doubleValue()+"天");
        });
    }


    // 矿工挖金币线程
    static class MinerProduceGoldenThread implements Runnable{
        private User user;
        public MinerProduceGoldenThread(User user) {
            if(user == null) throw new RuntimeException("user cannot be null");
            this.user = user;
        }

        @Override
        public void run() {
            BigDecimal seconds = new BigDecimal(3600 * 24);//一天
            int scale = 4;
            BigDecimal goldJar = new BigDecimal(0);//金币 存储罐
            goldJar.setScale(scale);
            while (true) {
                BigDecimal oldSpeed = new BigDecimal(user.getMoneyDepositSpeed())
                        .setScale(scale, BigDecimal.ROUND_HALF_UP);
                BigDecimal goldPerSecond = oldSpeed.divide(seconds,BigDecimal.ROUND_HALF_UP);//一秒产生金币数
                BigDecimal gold =  goldJar.add(goldPerSecond);
                int bill;
                if ( (bill = gold.intValue()) > 0) { // 整币
                    goldJar = gold.subtract(new BigDecimal(bill));
                    user.setMoneyDeposit(user.getMoneyDeposit()+bill); //取出整币
                    users.put(user.getSn(), user);
                }else{
                    goldJar = gold;
                }

                //每秒循环一次，取出金币
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // 用户
    static class User{
        private long sn;                        // 用户编号
        private String nick = "";               // 用户昵称
        private long moneySum = 10_000L;        // 总金币
        private long moneyDeposit;              // 矿厂金币
        private long moneyDepositSpeed;         // 每天产金数量
        private final long moneyDepositMax = 100;// 矿厂金币最大容量
        private ArrayList<Miner> uMiners = new ArrayList<>();// 矿工

        private User(long sn) {
            this.sn = sn;
        }

        public static User newUser(){
            long sn = sn_curr.getAndAdd(1);
            User currUser = new User(sn);
            users.put(sn, currUser);
            return currUser;
        }

        public synchronized Miner.MinerInfo newMiner(){//按顺序添加矿工,金币足够方能添加
            int minerType = uMiners.size();
            if(minerType >= miners.size()){
                return Miner.MinerInfo.FAIL(1,"minerType should be sum miner size");
            }
            Miner miner = miners.get(minerType);
            if(miner == null){
                UtilLogger.printSplitLine("miner null",minerType,miners.size());
            }
            if (miner.golden > this.moneySum) {
                return Miner.MinerInfo.FAIL(1,"moneyDeposit sum not enough");
            }
            this.moneySum -= miner.golden;
            this.moneyDepositSpeed += miner.speed;
            this.uMiners.add(miner);
            return Miner.MinerInfo.SUCCESS(miner);

        }

        public long getSn() {
            return sn;
        }

        public void setSn(long sn) {
            this.sn = sn;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public long getMoneySum() {
            return moneySum;
        }

        public void setMoneySum(long moneySum) {
            this.moneySum = moneySum;
        }

        public long getMoneyDeposit() {
            return moneyDeposit;
        }

        public void setMoneyDeposit(long moneyDeposit) {
            this.moneyDeposit = moneyDeposit;
            if(this.moneyDeposit >= moneyDepositMax){
                this.moneySum += moneyDepositMax;
                this.moneyDeposit -= moneyDepositMax;
            }
        }

        public long getMoneyDepositSpeed() {
            return moneyDepositSpeed;
        }

        public void setMoneyDepositSpeed(long moneyDepositSpeed) {
            this.moneyDepositSpeed = moneyDepositSpeed;
        }

        public ArrayList<Miner> getUMiners() {
            return uMiners;
        }

        public void setUMiners(ArrayList<Miner> uMiners) {
            this.uMiners = uMiners;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            User user = (User) o;
            return sn == user.sn;
        }

        @Override
        public int hashCode() {
            return Objects.hash(sn);
        }

        @Override
        public String toString() {
            return "User{" +
                    "sn=" + sn +
                    ", nick='" + nick + '\'' +
                    ", " + moneySum +
                    "/" + moneyDeposit +
                    "/" + moneyDepositSpeed +
                    ", " + uMiners.size() +
                    '}';
        }
    }

    // 矿工
    public enum Miner{
        MINER_0000(0,  300,   1500),
        MINER_0001(1, 1000,   3500),
        MINER_0002(2, 5000,  15000),
        MINER_0003(3, 10000, 20000),
        MINER_0004(4, 50000, 80000),
        MINER_0005(5,100000,120000);
        private int type;
        private long golden;//投入
        private long speed;//产出速率
        Miner(int type,long golden, long speed){
            this.type = type;
            this.golden = golden;
            this.speed = speed;
        }

        public static ArrayList<Miner> initMiners(){
            ArrayList<Miner> miners = new ArrayList<>();
            miners.add(Miner.MINER_0000);
            miners.add(Miner.MINER_0001);
            miners.add(Miner.MINER_0002);
            miners.add(Miner.MINER_0003);
            miners.add(Miner.MINER_0004);
            miners.add(Miner.MINER_0005);
            return miners;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public long getGolden() {
            return golden;
        }

        public void setGolden(long golden) {
            this.golden = golden;
        }

        public long getSpeed() {
            return speed;
        }

        public void setSpeed(long speed) {
            this.speed = speed;
        }

        static class MinerInfo{
            private int code;
            private String msg;
            private Miner miner;

            public MinerInfo(int code, String msg, Miner miner) {
                this.code = code;
                this.msg = msg;
                this.miner = miner;
            }

            public static MinerInfo SUCCESS(Miner miner) {
                return new MinerInfo(0, "success", miner);
            }

            public static MinerInfo FAIL(int code, String msg) {
                if(code <= 0) {
                    throw new RuntimeException("fail code not permit here");
                }
                return new MinerInfo(code, msg,null);
            }

            public int getCode() {
                return code;
            }

            public void setCode(int code) {
                this.code = code;
            }

            public String getMsg() {
                return msg;
            }

            public void setMsg(String msg) {
                this.msg = msg;
            }

            public Miner getMiner() {
                return miner;
            }

            public void setMiner(Miner miner) {
                this.miner = miner;
            }

            @Override
            public String toString() {
                return "MinerInfo{" +
                        "code=" + code +
                        ", msg='" + msg + '\'' +
                        ", miner=" + miner +
                        '}';
            }
        }
    }

}
