package cn.pheker.test;

import java.util.BitSet;

/**
 * @author  cn.pheker
 * @date    2019/3/4 21:39
 * @email   1176479642@qq.com
 * @desc    简单实现布隆过滤器
 */
public class BloomFilter {
    
    
    public static BloomFilter build(){
        return new BloomFilter();
    }
    
    private BloomFilter() {}
    
    /**
     * bit位总数
     */
    private int SIZE = 1 << 24;
    
    /**
     * bits数组
     */
    BitSet bitSet = new BitSet(SIZE);
    
    /**
     * hash种子，每个种子对应一个hash函数；如下可知共有8个hash函数
     */
    private int[] seeds = new int[]{3,5,7,9,11,13,17,19};
    
    /**
     * 存储的string个数
     */
    int size;
    
    /**
     * hash函数
     * @param value
     * @param seed
     * @return
     */
    private int hash(String value, int seed) {
        int hash = 0;
        int len = value.length();
        for (int i = 0; i < len; i++) {
            hash += seed*i;
        }
        // 防止溢出
        return hash & (SIZE - 1);
    }
    
    /**
     * 添加
     * @param value
     */
    public void add(String value) {
        for (int seed :seeds) {
            int hash = hash(value, seed);
            bitSet.set(hash);
        }
        size++;
    }
    
    /**
     * 是否可能存在
     * @param value
     * @return
     */
    public boolean exist(String value) {
        boolean exist = true;
        for (int seed :seeds) {
            int hash = hash(value, seed);
            exist &= bitSet.get(hash);
        }
        return exist;
    }
    
    public int size(){
        return size;
    }
    
    
    public static void main(String[] args) {
        BloomFilter bf = BloomFilter.build();
        bf.add("java");
        bf.add("alibaba");
        bf.add("itslulo");
    
        // 存了多少
        int size = bf.size();
        System.out.println("size = " + size);
        
        // 可能存在
        boolean alibaba = bf.exist("alibaba");
        System.out.println("alibaba = " + alibaba);
    
        // 一定不存在
        boolean pheker = bf.exist("cn.pheker");
        System.out.println("pheker = " + pheker);
    }
}
