package cn.pheker.test;

import cn.pheker.utils.UtilElapsedTime;
import cn.pheker.utils.UtilLogger;

import java.util.*;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/7/25 20:11
 * email     1176479642@qq.com
 * desc      求坏数最少的解(坏数：数组中不符合升序的数)
 *
 *           算法复杂度n*m, n为原始数据长度,
 *           m为假定原始数据第一个元素为好数时计算得到的badIndices(坏数索引数组)的长度,
 *           由于0<= m < n, 则n< m*n <n*n
 *           求解的过程中可能有多个可行解，故复杂度要比n*m高些,但还是会小于n*n
 * </pre>
 */
public class BadNumbersOptimumSolution {
    
    public static void main(String[] args) {
        UtilElapsedTime.test(()->{
            
            UtilLogger.printSplitLine("原始数据");
//            List<Integer> nums = generateNums(100);// 生成随机数的方式-- 坏数太多
            List<Integer> nums = Arrays.asList(new Integer[]{1,2,3,4,6,5,7,100,99,1,2,3,4,6,5,99,100,97,98});//1,2,4,7,6,5,22,35
            UtilLogger.echoList(nums);
            
            UtilLogger.printSplitLine("最优解(坏数索引)");
            List<List<Integer>> optimumSolutionsIndices = getOptimumSolutionsIndices(nums);
            UtilLogger.echoList(optimumSolutionsIndices);
    
            /*原始数据(标明坏数)*/
            UtilLogger.printSplitLine("最优解数据(标明坏数)");
            int numSize = nums.size();
            List<List<String>> shows = new ArrayList<>();
            int size = optimumSolutionsIndices.size();
            for (int i = 0; i < size; i++) {
                List<Integer> badIndices = optimumSolutionsIndices.get(i);
                int badIndicesSize = badIndices.size();
                if(size == 0) continue;
                int currIndexOfBadIndices = 0;
                List<String> show = new ArrayList<>();
                for (int j = 0; j < numSize; j++) {
                    String num = nums.get(j).toString();
                    if(currIndexOfBadIndices < badIndicesSize &&
                            j == badIndices.get(currIndexOfBadIndices)) {
                        num = "$---"+num;
                        currIndexOfBadIndices++;
                    }
                    show.add(num);
                }
                shows.add(show);
            }
            UtilLogger.echoList(shows);
            return "耗时";
        });
    }
    
    /**
     * 获取最优解，可能多个
     * @param nums 原始数据
     * @return 最优解(内含坏数索引数组)
     */
    private static List<List<Integer>> getOptimumSolutionsIndices(List<Integer> nums) {
        List<List<Integer>> multiIndices = new ArrayList<>();//多个最优解
        int size = nums.size();
        if(size < 2) return multiIndices;
    
        // assume first is good number
        List<Integer> firstBadIndices = getBadIndices(nums, 0);
        multiIndices.add(firstBadIndices);
        
        // 只去假定坏数为good number(利用连续好数进行优化)
        int optimumSize = firstBadIndices.size();
        int badSize = firstBadIndices.size();
        for (int i = 0; i < badSize; i++) {
            //assume bad number is good number;
            Integer badIndexAsGoodIndex = firstBadIndices.get(i);
            List<Integer> currBadIndices = getBadIndices(nums, badIndexAsGoodIndex);
            int currSize = currBadIndices.size();
            //多个最优解,排除已存在的最优解
            if(optimumSize == currSize
                    && !listInList(multiIndices, currBadIndices)){
                multiIndices.add(currBadIndices);
            }else if (currSize < optimumSize) { //更优解
                multiIndices.clear();
                multiIndices.add(currBadIndices);
                optimumSize = currSize;
            }
        }
        return multiIndices;
    }
    
    /**
     * 找出坏数索引
     * @param nums 原始数据
     * @param index 好数索引
     * @return
     */
    private static List<Integer> getBadIndices(List<Integer> nums, int index) {
        // 二分求坏数可行解, 分界点为假定的好数(索引为index)
        List<Integer> badIndices = new ArrayList<>();
        // before
        int goodIndex = index;
        for (int i = index-1; i >= 0; i--) {
            if (nums.get(i) <= nums.get(goodIndex)) {
                goodIndex = i;
            }else{
                badIndices.add(i);
            }
        }
    
        Collections.reverse(badIndices);
        
        // after
        int size = nums.size();
        goodIndex = index;
        for (int i = index+1; i < size; i++) {
            if (nums.get(goodIndex) <= nums.get(i)) {
                goodIndex = i;
            }else{
                badIndices.add(i);
            }
        }
        return badIndices;
    }
    
    /**
     * 生成原始数据，范围1-i
     * @param i 范围最大值
     * @return  原始数据
     */
    private static List<Integer> generateNums(int i) {
        ArrayList<Integer> nums = new ArrayList<>();
        Random random = new Random();
        for (int j = 0; j < i; j++) {
            nums.add(random.nextInt(i) + 1);
        }
        return nums;
    }
    
    
    /**
     * List container中是否已存在List elem
     * @param container List
     * @param elem      element
     * @return
     */
    public static boolean listInList(List<List<Integer>> container, List<Integer> elem) {
        for (List<Integer> indices: container){
            if (listEquals(indices, elem)) return true;
        }
        return false;
    }
    /**
     * 比较两个list是否完全相同
     * @param one
     * @param two
     * @return
     */
    public static boolean listEquals(List<Integer> one, List<Integer> two) {
        int oneSize;
        if((oneSize = one.size()) != two.size()) return false;
        for (int i = 0; i < oneSize; i++) {
            if (one.get(i) != two.get(i)) return false;
        }
        return true;
    }
    
}
