package cn.pheker.test;

import cn.pheker.utils.UtilElapsedTime;
import cn.pheker.utils.UtilLogger;

import java.util.*;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/4/26 14:29
 * email     1176479642@qq.com
 * desc      链表中有A,B,C,D四个元素Entry，且Entry属性item可指向其它Entry，A指向C，D批向B。
 *           请拷贝此链表,且注意效率 时间复杂度小于log(n^2)
 *
 -------------------- src --------------------
 [A->C, B, C, D->B]
 -------------------- target after copy src --------------------
 [A1->C1, B1, C1, D1->B1]
 [21.831123ms] 耗时

 *
 * </pre>
 */
public class SpecialLinkedListCopy {

    public static void main(String[] args) {
        UtilElapsedTime.test(() -> new SpecialLinkedListCopy().test());
    }

    //TODO 我在想...最后要不要考虑 循环引用的问题
    private String test() {
        Entry a = new Entry("A"),
            b = new Entry("B"),
            c = new Entry("C"),
            d = new Entry("D");
//        c.setItem(b);
        a.setItem(c);
        d.setItem(b);

        List<Entry> src = Arrays.asList(new Entry[]{a,b,c,d});
        UtilLogger.printSplitLine("src",src);

        List<Entry> target = new LinkedList();
        Iterator<Entry> srcIte = src.iterator();
        while (srcIte.hasNext()) {
            Entry curr = srcIte.next();
            Entry currCopy = (Entry) curr.clone();

            Entry currItem = curr.getItem();
            currCopy.setItem(currItem);
            curr.setItem(currCopy);

            target.add(currCopy);
        }

        Iterator<Entry> targetIte = target.iterator();
        while (targetIte.hasNext()) {
            Entry curr = targetIte.next();
            Entry item = curr.getItem();

            Entry itemLast = item;
            while(item != null && !item.equals(curr)){
                itemLast = item;
                item = item.getItem();
            }
            curr.setItem(itemLast);
        }
//        UtilLogger.printSplitLine("target",target);
//        UtilLogger.printSplitLine("src after copy", src);

        UtilLogger.printSplitLine("target after copy src", target);

        return "耗时";
    }

    class Entry implements Cloneable{
        private String key;
        private Entry item;

        public Entry(String key) {
            this.key = key;
        }

        public Entry(String key, Entry item) {
            this.key = key;
            this.item = item;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }


        public Entry getItem() {
            return item;
        }

        public void setItem(Entry item) {
            this.item = item;
        }

        @Override
        protected Object clone() {
            Entry deepCopy = null;
            try {
                deepCopy = (Entry) super.clone();
                String key = deepCopy.getKey();
                if(key.matches(".*_\\d")){
                    String[] split = key.split("_(?=\\d+$)");
                    Integer num = Integer.valueOf(split[split.length - 1]);
                    deepCopy.setKey(split[0]+(++num));
                }else{
                    deepCopy.setKey(key+"1");
                }
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            if(item != null) {
                Entry copyItem = (Entry) item.clone();
                deepCopy.setItem(copyItem);
            }
            return deepCopy;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Entry entry = (Entry) o;
            return Objects.equals(key, entry.key);
        }

        @Override
        public int hashCode() {
            return Objects.hash(key);
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder(key);
            Entry item = this;
            while( null != (item = item.getItem()) && !item.equals(this)){
                sb.append("->" + item.key);
            }
            if (item != null) {
                sb.append("->" + item.key);
            }
            return sb.toString();
        }

    }
}
