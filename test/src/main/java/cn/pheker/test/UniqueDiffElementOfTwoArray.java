package cn.pheker.test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author cn.pheker
 * @date 2019/2/27 23:38
 * @email 1176479642@qq.com
 * @desc 两个数组只有一个数组不同，找出来
 */
public class UniqueDiffElementOfTwoArray {
    
    
    public static void main(String[] args) {
        int[] first = new int[]{2, 5, 33, 678, 100, 256, 0};
        int[] two = new int[]{33, 678, 33, 99, 0, 2, 256, 100};
        Integer diffElement = getDiffElement(first, two);
        System.out.println("diffElement = " + diffElement);
    
    }
    
    /**
     * 找出两个数组中唯一一个不同的元素
     * (bug: 长度差只有1，不同元素有多个时。所以前提是两个数组有且仅有一个不同元素)
     * @param first 数组
     * @param two   数组
     * @return  如果返回null说明两个数组并不是只有一个不同
     */
    private static Integer getDiffElement(int[] first, int[] two) {
        Integer diffElement = null;
        int[] minArr = two;
        int maxLen = first.length, twoLen = two.length;
        Map<Integer,Integer> maxMap = new HashMap<>(Math.max(maxLen, twoLen));
        
        // 1.put all elements of the longest array into map
        if (Math.abs(maxLen - twoLen) != 1) {
//            throw new RuntimeException("两个数组并不是只有一个不同");
            return null;
        } else if (maxLen > two.length) {
            for (int el : first) {
                maxMap.put(el, el);
            }
        } else {
            for (int el : two) {
                maxMap.put(el, el);
            }
            minArr = first;
        }
        
        // 2.find the different element via remove element of the shortest array
        // from map if exists. if not exists, it's the right answer.
        for (int el :minArr) {
            if(maxMap.remove(el) == null){
                diffElement = el;
                break;
            }
        }
        return diffElement;
    }
    
}
