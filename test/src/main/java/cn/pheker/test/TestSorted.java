package cn.pheker.test;

import cn.pheker.utils.UtilBean;
import cn.pheker.utils.UtilLogger;

import java.util.List;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/6/1 21:26
 * email     1176479642@qq.com
 * desc
 *
 * </pre>
 */
public class TestSorted {
    
    
    public static void main(String[] args) {
    
        List<String> strList = UtilBean.batchInsert(null, "1A", "2A", "2C", "5B", "3A");
        strList.sort((x,y) -> {
            int letter = x.charAt(1) - y.charAt(1);
            if(letter == 0) {
                return x.charAt(0) - y.charAt(0);
            }
            return letter;
        });
    
        UtilLogger.println(strList);
    }
}
