package cn.pheker.test;

import cn.pheker.utils.UtilElapsedTime;
import cn.pheker.utils.UtilLogger;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/4/23 14:25
 * email     1176479642@qq.com
 * desc      查找数组重复元素或去重
 *
 * </pre>
 */
public class DuplicateElements {

    private static final List<Integer> list = Stream.of(1, 3, 4, 4, 6, 10, 9, 9, 22, 22, 5,22)
            .collect(Collectors.toList());

    public static void main(String[] args) {
        UtilElapsedTime.test(() ->{
                test();
                return "test";
        });
        UtilElapsedTime.test(() ->{
                test2();
                return "test2";
        });
        UtilElapsedTime.test(() ->{
                getUniqueList();
                return "getUniqueList";
        });
    }

    private static void getUniqueList() {
        UtilLogger.printSplitLine("getUniqueList");
        List<Integer> source = Stream.of(1, 3, 4, 4, 6, 10, 9, 9, 22, 22, 5,22)
                .collect(Collectors.toList());
        List<Integer> duplicates = new ArrayList<>();
        Iterator<Integer> ite = source.iterator();
        while (ite.hasNext()) {
            Integer curr = ite.next();
            if (duplicates.contains(curr)) {
                ite.remove();
            }else{
                duplicates.add(curr);
            }
        }
        UtilLogger.println(duplicates);
    }

    /**
     * 直接迭代查找重复元素
     */
    private static void test2() {
        List<Integer> ret = new ArrayList<>();
        Iterator<Integer> ite = list.iterator();
        while (ite.hasNext()) {
            Integer next = ite.next();
            ite.remove();//排除当前元素
            if (list.contains(next) && !ret.contains(next)) {
                ret.add(next);
            }
        }
        UtilLogger.printSplitLine("test2",ret);
    }

    /**
     * 先排序后查找重复元素
     */
    private static void test() {
        List<Integer> list2 = list;
        UtilLogger.println(list2);
        Collections.sort(list2);
        Set listRepeat = new HashSet<>();
        for (int i = 0; i < list2.size()-1; i++) {
            Integer curr = list2.get(i);
            Integer next = list2.get(i + 1);
            if (curr.equals(next)) {
                listRepeat.add(curr);
            }
        }
        UtilLogger.printSplitLine("test",listRepeat);
    }
}
