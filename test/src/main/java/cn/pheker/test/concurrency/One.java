package cn.pheker.test.concurrency;

import java.util.concurrent.*;

import static cn.pheker.utils.UtilLogger.println;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/7/7 12:13
 * email     1176479642@qq.com
 * desc
 *
 * </pre>
 */
public class One {
    
    public static int clientTotal = 5000;
    public static int threadTotal = 100;
    public static int userReqSum = 1000;
    
    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newCachedThreadPool();
        Semaphore semaphore = new Semaphore(threadTotal);//用于限制线程并发量
        CountDownLatch countDownLatch = new CountDownLatch(threadTotal);
        
        final int maxCount = clientTotal;
        OpAdd opAdd = new OpAdd(maxCount);
    
        for (int i = 0; i < userReqSum; i++) {//userReqSum次请求,每次请求相当于一个线程
            executorService.execute(() -> {
                try {
                    semaphore.acquire();//用于限制线程并发量
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                opAdd.add();
                semaphore.release();
                countDownLatch.countDown();
            });
        }
        showBlockedUserReq(semaphore);
        countDownLatch.await();//所有请求完成后
        executorService.shutdown();
        println(opAdd.getCount());
    }
    
    private static void showBlockedUserReq(Semaphore semaphore) {
        while (true) {
            int queueLength = semaphore.getQueueLength();
            if (queueLength >= 0) {
                break;
            } else if (queueLength < userReqSum - threadTotal) {
                println(queueLength);
            }
        }
    }
    
    //自增操作类
    static class OpAdd implements Runnable{
        private int count = 0;
        private int maxCount = 0;
        Object lock = new Object();
    
        public OpAdd(int maxCount) {
            this.maxCount = maxCount;
        }
        
    
        @Override
        public void run() {
            add();
        }
    
        private void add() {
            while(true && count < maxCount) {
                synchronized (lock) {
                    if (count < maxCount) {
                        count++;
                        try {
                            TimeUnit.MILLISECONDS.sleep(1);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    
        public int getCount() {
            return count;
        }
    }
}
