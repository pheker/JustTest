package cn.pheker.test;

import cn.pheker.utils.BooleanArray;
import cn.pheker.utils.Range;
import cn.pheker.utils.UtilElapsedTime;
import cn.pheker.utils.UtilLogger;

import java.util.Iterator;
import java.util.stream.Stream;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/5/2 14:22
 * email     1176479642@qq.com
 * desc
 *
 * </pre>
 */
public class TestBooleanArray {

    public static void main(String[] args) {
        test();
        testMethod();
    }

    private static void testMethod() {
        UtilLogger.printSplitLine("测试性能");
        BooleanArray bs = BooleanArray.getInstance();
        Stream<Long> x = Range.newRange(10000);
        Stream<Long> y = Range.newRange(10000);
        Stream<Long> z = Range.newRange(10000);
        Stream<Long> zz = Range.newRange(10000);
        UtilElapsedTime.test(()->{
            x.forEach(i -> bs.push(true));
            return "put "+bs.size();
        });

        UtilElapsedTime.test(()->{
            y.forEach(i -> bs.get(i.intValue()));
            return "get "+bs.size();
        });


        UtilElapsedTime.test(()->{
            bs.each(a-> {

            });
            return "each "+bs.size();
        });

        UtilElapsedTime.test(()->{
            bs.clear();
            return "clear "+bs.size();
        });


        z.forEach(i->bs.push(true));
        //删除太慢
        UtilElapsedTime.test(()->{
            zz.forEach(i -> bs.remove(0));
            return "remove "+bs.size();
        });
    }

    private static void test() {
        //初始化
        BooleanArray ba = BooleanArray.getInstance();
        new BooleanArray(false, true);
        new BooleanArray(new Boolean[]{false,true});

        //crud
        BooleanArray bs = BooleanArray.getInstance();
        int index = bs.push(true);
        boolean one = bs.get(0);
        int index2 = bs.push(false);
        boolean two = bs.get(1);
        int index3 = bs.push(true);
        boolean three = bs.get(2);
//        bs.set(3, false);// IndexOutOfBoundsException(3/2)
        boolean flag = bs.remove(1);

        UtilLogger.println(bs,flag);

        UtilLogger.printSplitLine("遍历");
        //遍历
        //1
        Iterator<Boolean> ite = bs.iterator();
        while (ite.hasNext()) {
            Boolean next = ite.next();
            UtilLogger.println(next);
        }

        bs.set(0, false);
        //2
        bs.each(x -> UtilLogger.println(x));
    }

}
