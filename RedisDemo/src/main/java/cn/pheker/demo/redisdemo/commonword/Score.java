package cn.pheker.demo.redisdemo.commonword;

import cn.pheker.utils.UtilLogger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/3/30 12:30
 * email     1176479642@qq.com
 * desc
 *
 * </pre>
 */
public class Score {
    public static List<String> score5 = Arrays.asList("Q","A","Z","P","'");
    public static List<String> score4 = Arrays.asList("W","S","X","O","L");
    public static List<String> score3 = Arrays.asList("E","D","C","I","K");
    public static List<String> score2 = Arrays.asList("T","G","B","Y","H","N");
    public static List<String> score1 = Arrays.asList("R","F","V","Y","H","M");
    
    public static Double getSingleScore(char ch){
        String c = (ch+"").toUpperCase();
        if (score5.contains(c)) {
            return 5.0;
        }else if (score4.contains(c)) {
            return 4.0;
        }else if (score3.contains(c)) {
            return 3.0;
        }else if (score2.contains(c)) {
            return 2.0;
        }else if (score1.contains(c)) {
            return 1.0;
        }
        return 0.0;
    }
    
    public static Double getSumScore(String word) {
        double sum = 0.0;
        int len = word.length();
        for (int i = 0; i < len; i++) {
            sum += getSingleScore(word.charAt(i));
        }
        BigDecimal bigDecimal = new BigDecimal(sum/(5.0*len));
        return bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }
    
    public static void main(String[] args) {
        UtilLogger.println(getSumScore("abcpliwe"));
    }
}