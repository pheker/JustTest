package cn.pheker.demo.redisdemo.commonword;

import cn.pheker.demo.redisdemo.UtilJedis;
import cn.pheker.utils.*;
import redis.clients.jedis.Jedis;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/3/30 10:45
 * email     1176479642@qq.com
 * desc
 *
 * </pre>
 */
public class Scan {
    
    private static int count = 0;
    private static String folder = "D:\\IntelliJ";
    
    public static void main(String[] args) {
        UtilElapsedTime.test( ()->{
            start(folder);
            return "统计并保存成功: "+count;
        });
    }

    public static List allowFileExt = Arrays.asList(".java",".kt");
    public static void start(String rootDir){
        Jedis jedis = UtilJedis.getJedis();
//        jedis.flushDB();//删除所有
        List<String> files = UtilFile.getAllFileNameInFold(rootDir);
        files.stream()
                .filter(path-> allowFileExt.contains(UtilFile.getFileExt(path).toLowerCase()))
                .forEach(path -> {
                    UtilLogger.printSplitLine(UtilFile.getFileName(path));
                    String content = "";
                    content = UtilFile.readLogByList(path).stream().reduce(content, (a, b) -> a + b);
                    getWords(content).stream()
                            .forEach(word ->{
                                double score = Score.getSumScore(word);
                                String json = Word.json(word, score);
                                jedis.set(word, json);
                            });
                });
        UtilLogger.println(count);
        saveWord(jedis);
    }
    
    public static List<String> getWords(String content){
        List<String> words = new ArrayList<String>();
        Pattern p = Pattern.compile("\\w+");
        Matcher matcher = p.matcher(content);
        while (matcher.find()) {
            String group = matcher.group();
            if(group.contains("_") || UtilRegexp.containUpperCase(group)) {
                String[] split = group.split("_|(?=[A-Z])");
                int length = split.length;
                for (int i = 0; i < length; i++) {
                    String w = split[i];
                    if (UtilRegexp.isNullOrEmpty(w)
                            || w.length() < 2
                            || w.length() > 20
                            || UtilRegexp.containNumbers(w)) {
                        continue;
                    }
                    words.add(split[i].toLowerCase());
                    count++;
                }
            }
        }
        return words;
    }
    
    public static void saveWord(Jedis jedis) {
        String wordList = "";
        Set<String> result = jedis.keys("*");
        wordList = result.stream()
                .sorted()
                .map(key -> {
                    Word word = null;
                    try {
                        word = (Word) UtilJson.toObject(jedis.get(key), Word.class);
                    } catch (Exception e) {
                        UtilLogger.println(key);
                    }
                    return word == null?"":word.getWord() + "\r\n";
                }).reduce(wordList, (a, b) -> a + b);
        File file = new File(folder + "\\wordList.txt");
        UtilFile.writeString(file,wordList,false);
    
    }
    
}
