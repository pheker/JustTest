package cn.pheker.demo.redisdemo;

import cn.pheker.utils.UtilLogger;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/3/16 17:14
 * email     1176479642@qq.com
 * desc      测试Redis的数据类型
 *
 * </pre>
 */
public class TestDataType {
    
    public static void main(String[] args) {
        Jedis jedis = UtilJedis.getJedis();
        
        //map
        if(!jedis.exists("name")) {
            jedis.set("name", "cn.pheker");
        }else{
            String name = jedis.get("name");
            UtilLogger.println(name);
        }
        
        Long age = jedis.incr("age");
        UtilLogger.println(age);
        
        //list
        jedis.lpush("languages", "java");
        jedis.lpush("languages", "python","javascript");
        List<String> languages = jedis.lrange("languages", 0, 10);
        languages.stream().forEach(System.out::println);
        jedis.del("languages");
    
        jedis.hmset("strMap", new HashMap<String,String>(){
            {
                put("a", String.valueOf(1));
                put("b", String.valueOf(2));
                put("c", String.valueOf(3));
            }
        });
        List<String> strList = jedis.hmget("strMap","a","b");
        strList.stream().forEach(System.out::println);
    
    
        showKeys(jedis);
    }
    
    
    public static void showKeys(Jedis jedis) {
        UtilLogger.println("-------------keys----------------");
        jedis.keys("*").stream().forEach(k -> UtilLogger.println(k));
    }
}
