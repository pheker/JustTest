package cn.pheker.demo.redisdemo;

import cn.pheker.utils.UtilLogger;
import redis.clients.jedis.Jedis;

import java.io.*;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/3/16 17:06
 * email     1176479642@qq.com
 * desc      Jedis的工具类
 *
 * </pre>
 */
public class UtilJedis {

    private static volatile Jedis jedis;
    private static Object lock = new Object();
    
    public static Jedis getJedis(){
        if (jedis == null) {
            synchronized (lock) {
                if (jedis == null) {
                    jedis = new Jedis();
                }
            }
        }
        return jedis;
    }
    
    public static byte[] serialize(Object obj){
        byte[] bytes = null;
        ByteArrayOutputStream baos = null;
        ObjectOutputStream oos = null;
        try {
            baos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baos);
            oos.writeObject(obj);
            bytes=baos.toByteArray();
            baos.close();
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bytes;
    }
    public static Object deSerialize(byte[] bytes){
        Object obj=null;
        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
            ObjectInputStream ois = new ObjectInputStream(bais);
            obj=ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }
    

}
