package cn.pheker.demo.redisdemo.commonword;

import cn.pheker.demo.redisdemo.UtilJedis;
import cn.pheker.utils.UtilJson;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/3/30 11:31
 * email     1176479642@qq.com
 * desc
 *
 * </pre>
 */
public class Word implements Serializable{
    private String word ;    //英文
    private Double score;   //难度
    private String mean = "";    //中文含义
    private int count = 1;      //使用次数
    
    public Word(String word, Double score, int count) {
        this.word = word;
        this.score = score;
        this.count = count;
    }
    
    public Word(String word, Double score) {
        this.word = word;
        this.score = score;
    }
    
    public static String json(String word, Double score) {
        return UtilJson.toJson(new Word(word, score));
    }
    
    public String getWord() {
        return word;
    }
    
    public void setWord(String word) {
        this.word = word;
    }
    
    public Double getScore() {
        return score;
    }
    
    public void setScore(Double score) {
        this.score = score;
    }
    
    public String getMean() {
        return mean;
    }
    
    public void setMean(String mean) {
        this.mean = mean;
    }
    
    public int getCount() {
        return count;
    }
    
    public void setCount(int count) {
        this.count = count;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word1 = (Word) o;
        return Objects.equals(word, word1.word);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(word);
    }
    
    @Override
    public String toString() {
        return word+"\t"+score+"\t"+count+"\t"+mean;
    }
    
    public Map<String, String> toMap() {
        Map<String, String> map = Collections.emptyMap();
        map.put("word", word);
        map.put("score", score+"");
        map.put("mean", mean);
        map.put("count", count+"");
        return map;
    }
    
}