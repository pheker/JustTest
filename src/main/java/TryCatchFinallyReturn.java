import cn.pheker.utils.UtilLogger;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/3/19 23:06
 * email     1176479642@qq.com
 * desc      温习Try,Catch,finally中有return的情况
 * 参考链接：http://blog.csdn.net/stduent_/article/details/60955365
 *
 * </pre>
 */
public class TryCatchFinallyReturn {
    
    public static void main(String[] args) {
        UtilLogger.println(test());
        UtilLogger.println("------------test2-------------");
        UtilLogger.println(test2());
        UtilLogger.println("------------test3-------------");
        UtilLogger.println(test3());
    }
    
    
    public static int test(){//3
        int x = 1;
        try {
            return ++x/0;
        } catch (Exception e) {
            UtilLogger.println(e.getMessage(),x);//2
            return ++x;
        }finally {
            UtilLogger.println("finally",x);//3 返回3
            ++x;
            UtilLogger.println("finally",x);//4
        }
    }

    public static int test2(){//2
        int x = 1;
        try {
            return ++x;
        } catch (Exception e) {
            return ++x;
        }finally {
            UtilLogger.println("finally");
            ++x;
        }
    }
    
    public static int test3(){//3
        int x = 1;
        try {
            return ++x;
        } catch (Exception e) {
            return ++x;
        }finally {
            UtilLogger.println("finally");
            return ++x;
        }
    }
    
    //1.finally 一定会执行
    //2.先执行finally再执行return
    //3.finally中有return会忽略try中的return，但try中的++x依然会执行
    //4.如果finally中没return而try中有return,
    //  那么先执行try中的++x,并将返回值2保存到局部变量中,
    //  再执行finally中的++x,最后返回之前的局部变量2


}
