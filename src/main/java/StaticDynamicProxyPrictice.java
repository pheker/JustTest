import cn.pheker.utils.UtilLogger;
import org.omg.CORBA.SystemException;
import org.omg.CORBA.portable.InputStream;
import org.omg.CORBA.portable.InvokeHandler;
import org.omg.CORBA.portable.OutputStream;
import org.omg.CORBA.portable.ResponseHandler;

import java.io.File;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Scanner;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/3/21 8:59
 * email     1176479642@qq.com
 * desc      静态/动态代理实践
 *
 * </pre>
 */
class StaticDynamicProxyPractice {
    
    public static void main(String[] args) {
    
        UtilLogger.println("--------------static proxy-------------");
        // 静态代理,不易扩展,如果想代理多个类多个接口,
        // 只能不断添加实例引用、同名实例方法,而且还会遇到相同方法签名的问题
        StaticProxy staticProxy = new StaticProxy(new UserServiceImpl());
        staticProxy.save();
    
            // 静态代理 - 能过静态方法代理实例
//        StaticProxy proxy = StaticProxy.proxy(new UserServiceImpl());
//        proxy.save();
    
        
        UtilLogger.println("--------------dynamic proxy-------------");
        // 动态代理
        // 1.要实现InvocationHandler接口,并且只能代理接口
        // 2.通过反射invoke代理所有方法,比较灵活
        UserService userService = (UserService) DynamicProxy.proxy(new UserServiceImpl());
        userService.save();
    
        String s = new Scanner(System.in).nextLine();
    
    }

}



//静态代理类
class StaticProxy implements UserService{
    
    private final UserService service;
    
    public StaticProxy(UserService service) {
        this.service = service;
    }
    
    //private constructor + getInstance
    public static StaticProxy proxy(UserService service) {
        return new StaticProxy(service);
    }
    
    @Override
    public void save() {
        dbOpen();
        service.save();
        dbClose();
    }
    
    private void dbOpen(){
        UtilLogger.println("open database...");
    }
    
    private void dbClose(){
        UtilLogger.println("close database...");
    }
    
}

//业务接口
interface UserService{
    void save();
}
//业务实例
class UserServiceImpl implements UserService{
    
    @Override
    public void save() {
        UtilLogger.println("saving...");
    }
    
}

//动态代理类
class DynamicProxy implements InvocationHandler{
    
    private Object target;//要代理的业务对象
    private DynamicProxy(){ }
    
    private DynamicProxy(Object target) {
        this.target = target;
    }
    
    public static Object proxy(Object target){
        DynamicProxy dynamicProxy = new DynamicProxy(target);
        return Proxy.newProxyInstance(target.getClass().getClassLoader(),
                target.getClass().getInterfaces(), dynamicProxy);
    }
    
    
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        dbOpen();
        Object obj = method.invoke(target, args);//通过反射调用真实业务对象的方法
        dbClose();
        return  obj;
    }
    
    private void dbOpen(){
        UtilLogger.println("open database...");
    }
    
    private void dbClose(){
        UtilLogger.println("close database...");
    }
    

}