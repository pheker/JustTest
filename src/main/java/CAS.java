import cn.pheker.utils.UtilLogger;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/3/29 13:57
 * email     1176479642@qq.com
 * desc
 *
 * </pre>
 */
public class CAS {
    
    
    public static void main(String[] args) throws InterruptedException {
        UtilLogger.printSplitLine("newFixedThreadPool");
        test();
        Thread.sleep(200);
        UtilLogger.printSplitLine("newScheduledThreadPool");
        test2();
    }
    
    private static AtomicInteger count = new AtomicInteger(0);
    
    public static void test(){
        ExecutorService pool = Executors.newFixedThreadPool(2);
    
        for (int i : Arrays.asList(1,2,3)) {
            pool.execute(new PlusCASThread(i));
        }
        pool.shutdown();
        
    }
    
    public static void test2(){
        ExecutorService pool = Executors.newScheduledThreadPool(2);
        for (int i : Arrays.asList(1,2,3)) {
            pool.execute(new PlusCASThread(i));
        }
        pool.shutdown();
    }
    
    public static class PlusCASThread implements Runnable {
        private final int threadSn;
    
        public PlusCASThread(int threadSn) {
            this.threadSn = threadSn;
        }
    
        @Override
        public void run() {
            int i = count.addAndGet(1);
            UtilLogger.println("thread-"+threadSn+": "+i);
        }
    }

}
