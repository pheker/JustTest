import cn.pheker.utils.UtilLogger;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/3/20 22:57
 * email     1176479642@qq.com
 * desc      对数组有两条重要的泛型限制：
 *              首先，不能实例化类型为类型参数的的数组；
 *                  如：public void newInstance(T t){
 *                          T[] arr = new T[3];
 *                     }
 *              其次，不能创建特定类型的泛型引用数组。
 *                  如：new ArrayList<Integer>();
 * 链接：http://blog.csdn.net/lexang1/article/details/49620223
 *
 * </pre>
 */
public class NestedList {
    
    public static void main(String[] args) {
        getNestedList();
        getNestedList2();
        getNestedList3();
        getNestedList4();
        getTrueNestedList();
    
    }

    public static ArrayList<?>[] getNestedList(){
        ArrayList<Integer> list = new ArrayList<Integer>(){{add(1);}};
        ArrayList<?>[] nestedList = new ArrayList<?>[]{list, list};
        UtilLogger.println(nestedList);
        return nestedList;
    }
    public static List<?>[] getNestedList2(){
        ArrayList<Integer> list = new ArrayList<Integer>(){{add(1);}};
        List<?>[] nestedList = new List<?>[]{list, list};
        UtilLogger.println(nestedList);
        return nestedList;
    }
    
    public static List[] getNestedList3(){
        ArrayList<Integer> list = new ArrayList<Integer>(){{add(1);}};
        List[] nestedList = new List[]{list, list};
        UtilLogger.println(nestedList);
        return nestedList;
    }
    
    public static Object[] getNestedList4(){
        ArrayList<Integer> list = new ArrayList<Integer>(){{add(1);}};
        Object[] nestedList = new Object[]{list, list};
        UtilLogger.println(nestedList);
        return nestedList;
    }
    
    public static ArrayList<ArrayList<Integer>> getTrueNestedList(){
        ArrayList<Integer> list = new ArrayList<Integer>(){{add(1);}};
        ArrayList<ArrayList<Integer>> nestedList = new ArrayList<ArrayList<Integer>>(){{
            add(list);
            add(list);
        }};
        UtilLogger.println(nestedList);
        return nestedList;
    }
}
