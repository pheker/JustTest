import cn.pheker.utils.UtilElapsedTime;
import cn.pheker.utils.UtilLogger;

import java.util.*;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/3/17 16:12
 * email     1176479642@qq.com
 * desc      测试无法在遍历的同时删除Map中的元素
 *
 * </pre>
 */
public class TestMapRemove {
    
    public static void main(String[] args) {
        
        UtilElapsedTime.test(() -> test());
        
    }
    
    private static String test() {
    
        Map<String, String> map = new HashMap<String, String>(){
            {
                put("a", "1");
                put("b", "1");
                put("c", "1");
                put("d", "1");
            }
        };
        
         Map<String, String> map2 = new HashMap<String, String>(){
            {
                put("a", "1");
                put("b2", "1");
                put("c", "1");
                put("d2", "1");
            }
        };
    
        Set<String> mspks = map.keySet();
        Iterator<String> mapite = mspks.iterator();
        List<String> sameks = new ArrayList<String>();
        while (mapite.hasNext()) {
            String mapk = mapite.next();
            if (map2.containsKey(mapk)
                    && map.get(mapk).equals(map2.get(mapk))) {
                sameks.add(mapk);
                //don't remove elements here
            }
        }
        sameks.stream().forEach(k -> {
            map.remove(k);
            map2.remove(k);
        });
        UtilLogger.println(map);
        UtilLogger.println(map2);
    
        return "remove two map duplicate elements finished";
    }
    
}
