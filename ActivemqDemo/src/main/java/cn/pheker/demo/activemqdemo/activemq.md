

# 1.JMS和ActiveMQ简介
JMS即Java消息服务（Java Message Service）应用程序接口是一个Java平台中关于面向消息中间件（MOM）的API，用于在两个应用程序之间，或分布式系统中发送消息，进行异步通信。Java消息服务是一个与具体平台无关的API，绝大多数MOM提供商都对JMS提供支持。

# 2. ActiveMQ
## 2.1 目录结构
* activemq-all-5.5.0.jar:所有MQ JAR包的集合，用于用户系统调用 
* bin:其中包含MQ的启动脚本 
* conf:包含MQ的所有配置文件 
* data:日志文件及持久性消息数据 
* example:MQ的示例 
* lib:MQ运行所需的所有Lib 
* webapps:MQ的Web控制台及一些相关的DEMO

## 2.2 启动/停止 activemq start|stop
activeMQ会占用两个端口，一个是负责接收发送消息的tcp端口:61616，一个是基于web负责用户界面化管理的端口:8161

## 2.3 ActiveMQ中有两种类型的消息模型
* 点对点（point-to-point，简称PTP）Queue消息传递模型
* 发布/订阅（publish/subscribe，简称pub/sub）Topic消息传递模型
### 2.3.1 Queue(点对点PTP)
>在点对点的传输方式中，消息数据被持久化，每条消息都能被消费，没有监听QUEUE地址也能被消费，数据不会丢失，一对一的发布接受策略，保证数据完整。

### 2.3.2 Topic(发布订阅P/S)
>在发布订阅消息方式中，消息是无状态的，
不保证每条消息被消费，只有监听该TOPIC地址才能收到消息并消费，否则该消息将会丢失。
一对多的发布接受策略，可以同时消费多个消息。

## 2.4 注意
## 2.4.1 P/S模式需要订阅者先启动
## 2.4.2 几个主要的类
|JMS公共             |点对点域                |发布/订阅域              |
|-------------------|---------------------- |---------------------- |
|ConnectionFactory	|QueueConnectionFactory |TopicConnectionFactory |
|Connection	        |QueueConnection	    |TopicConnection        |
|Destination        |Queue	                |Topic                  |
|Session	        |QueueSession	        |TopicSession           |
|MessageProducer	|QueueSender	        |TopicPublisher         |
|MessageConsumer	|QueueReceiver	        |TopicSubscriber        |

## 2.5 消息类型
```
1. 文本消息
TextMessage message = session.createTextMessage(msg);

2. Map消息
MapMessage message = session.createMapMessage();

3.序列化消息
Staff staff = new Staff(1, "搬砖工"); // Staff必须实现序列化
ObjectMessage message = session.createObjectMessage(staff);

4.字节消息
String str = "BytesMessage 字节消息";
BytesMessage message = session.createBytesMessage();
message.writeBytes(str.getBytes());

5.流消息
String str = "StreamMessage 流消息";
StreamMessage message = session.createStreamMessage();
message.writeString(str);
message.writeInt(521);
```


## 2.6 消息确认模式
客户端成功接收一条消息的标志是这条消息被签收。成功接收一条消息一般包括如 
下三个阶段： 
1. 客户端接收消息； 
2. 客户端处理消息； 
3. 消息被确认。确认可以由ActiveMQ 发起，也可以由客户端发起，取决于Session 确认模式的设置。 
在带事务的 Session 中，确认自动发生在事务提交时。如果事务回滚，所有已经接收的消息将会被再次传送。 
在不带事务的Session 中，一条消息何时和如何被确认取决于Session 的设置。

|模式	|说明|
|-------|---|
|Session.AUTO_ACKNOWLEDGE	    |默认的消息确认模式， session 将按照MessageConsumer.receive（） 返回成功或者用于办法回调MessageListener.onMessage（） 返回成功来主动回执消息确认。
|Session.CLIENT_ACKNOWLEDGE	    |在这种模式下，客户端应用法度代码中显式调用Message.acknowledge（）办法，以确认该消息。
|Session.DUPS_OK_ACKNOWLEDGE	|该模式，jms session可以或许主动的回执消息确认，然则它是一种怠惰的消息确认模式，一旦应用体系故障，一些消息可能会被处理惩罚然则没有回执，一旦体系重启，则会呈现消息的重发题目。
这是一种最快的消息确认模式，然则，花费者必须处理惩罚反复的消息处理惩罚题目。（例如：如何侦测和丢弃重发的消息的处理惩罚逻辑）
|Session.SESSION_TRANSACTED	    |当应用事务时，会话会隐含SESSION_TRANSACTED模式。事务提交的反响，然后相当于消息的确认。
当应用JMS事务来发送一组消息（多个消息），交易模式是很是有效的。但避免应用事务发送一个消息，因为这会带来额外的开销提交或回滚事务。
ActiveMQSession.INDIVIDUAL_ACKNOWLEDGE	它不是一个标准的消息确认模式，很类&＃20284;与CLIENT_ACKNOWLEDGE，只可惜它的消息确认是一种办法回调。在完成消息处理惩罚后不会去向理惩罚消息确认的。


## 2.7 ActiveMQ支持两种事务

JMS 事务。使用Session的commint()和rollback()即可实现。（类似于JDBC Connection对象）
XA 事务。XASession,XAResource通过与消息中介（Message Broker）通信来实现。
事务原理 
在支持事务的session中，producer发送message时在message中带有transactionID。broker收到message后判断是否有transactionID，如果有就把message保存在transaction store中，等待commit或者rollback消息。所以ActiveMQ的事务是针对broker而不是producer,不管session是否commit,broker都会收到message。

如果producer发送模式选择了persistent，那么message过期后会进入死亡队列。在message进入死亡队列之前，ActiveMQ会删除message中的transaction ID，这样过期的message就不在事务中了，不会保存在transaction store中，会直接进入死亡队列。



# 3 链接
[http://blog.csdn.net/u010080215/article/details/53462420](http://blog.csdn.net/u010080215/article/details/53462420)