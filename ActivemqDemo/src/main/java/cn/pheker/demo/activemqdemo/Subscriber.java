package cn.pheker.demo.activemqdemo;

import cn.pheker.utils.Range;
import cn.pheker.utils.UtilLogger;

import javax.jms.*;


/**
 * <pre>
 * author    cn.pheker
 * date      2018/3/16 15:05
 * email     1176479642@qq.com
 * desc      消息主题订阅者
 *
 * </pre>
 */
public class Subscriber {
    
    public static void main(String[] args) {
        Range.newRange(2)
                .forEach(sn -> newSubscriber(sn));
    }
    
    private static void newSubscriber(long sn) {
        Connection conn = UtilActiveMq.getConn();
        try {
            conn.start();
            final Session session = conn.createSession(true, Session.AUTO_ACKNOWLEDGE);
            Topic topic = session.createTopic(UtilActiveMq.TOPIC_NAME);
            MessageConsumer consumer = session.createConsumer(topic);
            consumer.setMessageListener(message -> {
                TextMessage textMessage = (TextMessage) message;
                try {
                    UtilLogger.println("["+sn+"]:"+textMessage.getText());
                    session.commit();
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            });
            
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
    
    
}
