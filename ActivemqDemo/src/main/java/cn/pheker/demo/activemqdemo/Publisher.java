package cn.pheker.demo.activemqdemo;

import cn.pheker.utils.Range;

import javax.jms.*;
import java.util.stream.Collectors;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/3/16 14:49
 * email     1176479642@qq.com
 * desc      消息主题发布者
 *
 * </pre>
 */
public class Publisher {
    
    public static void main(String[] args) {
        Connection conn = UtilActiveMq.getConn();
        Session session = null;
        try {
            session = conn.createSession(true, Session.AUTO_ACKNOWLEDGE);
            Topic topic = session.createTopic(UtilActiveMq.TOPIC_NAME);
            MessageProducer producer = session.createProducer(topic);
            Session finalSession = session;
            UtilActiveMq.sendMessages(producer,
                    Range.newRange(10)
                            .map(sn -> {
                                TextMessage textMessage = null;
                                try {
                                    textMessage = finalSession.createTextMessage("msg:" + sn);
                                    Thread.sleep(100);
                                } catch (JMSException e) {
                                    e.printStackTrace();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                return textMessage;
                            })
                    .collect(Collectors.toList())
            );
            session.commit();
        } catch (JMSException e) {
            e.printStackTrace();
        }finally {
            UtilActiveMq.close(session,conn);
        }
    }

}
