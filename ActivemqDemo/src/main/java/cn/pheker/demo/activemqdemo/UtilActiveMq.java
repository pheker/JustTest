package cn.pheker.demo.activemqdemo;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.util.List;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/3/16 10:56
 * email     1176479642@qq.com
 * desc
 *
 * </pre>
 */
public class UtilActiveMq {
    
    private static final String USERNAME = ActiveMQConnection.DEFAULT_USER;
    private static final String PASSWORD = ActiveMQConnection.DEFAULT_PASSWORD;
    private static final String BROKER_URL = ActiveMQConnection.DEFAULT_BROKER_URL;
    
    private static ConnectionFactory factory;
    private static Connection conn;
    public static final String QUEUE_NAME = "Queue-1";
    public static final String TOPIC_NAME = "Topic-1";
    
    private static final Object lock = new Object();
    public static ConnectionFactory getFactory(){
        if (factory == null) {
            synchronized (lock) {
                if (factory == null) {
                    factory = new ActiveMQConnectionFactory(USERNAME, PASSWORD, BROKER_URL);
                }
            }
        }
        return factory;
    }
    public static Connection getConn(){
        try {
            conn = getFactory().createConnection();
        } catch (JMSException e) {
            e.printStackTrace();
        }
        return conn;
    }

    public static void close(Session session,Connection conn){
        if(session != null){
            try {
                session.close();
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
        if(conn != null){
            try {
                conn.close();
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }
    
    public static void sendMessages(final MessageProducer producer, List<TextMessage> messages) {
        messages.stream().forEach(m -> {
            try {
                producer.send(m);
            } catch (JMSException e) {
                e.printStackTrace();
            }
        });
    }
    
}
