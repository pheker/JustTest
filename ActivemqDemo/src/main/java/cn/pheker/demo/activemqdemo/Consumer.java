package cn.pheker.demo.activemqdemo;

import cn.pheker.utils.Range;
import cn.pheker.utils.UtilLogger;

import javax.jms.*;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/3/16 13:35
 * email     1176479642@qq.com
 * desc      消息消费者
 *
 * </pre>
 */
public class Consumer {
    
    public static void main(String[] args) {
        Range.newRange(2)
                .forEach(sn -> createSingleConsumer(sn));
    }
    
    public static void createSingleConsumer(long sn){
        Connection conn = UtilActiveMq.getConn();
        Session session = null;
        
        try {
            conn.start();
            session = conn.createSession(true, Session.AUTO_ACKNOWLEDGE);
            Destination dest = session.createQueue(UtilActiveMq.QUEUE_NAME);
            MessageConsumer consumer = session.createConsumer(dest);
            while (true) {
                TextMessage receive = (TextMessage) consumer.receive();
                UtilLogger.println("["+sn+"]:"+receive.getText());
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }finally {
            UtilActiveMq.close(session,conn);
        }
    }
    
    
}
