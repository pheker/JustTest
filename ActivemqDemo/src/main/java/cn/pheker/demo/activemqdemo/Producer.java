package cn.pheker.demo.activemqdemo;

import cn.pheker.utils.UtilLogger;

import javax.jms.*;
import javax.xml.soap.Text;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/3/16 11:04
 * email     1176479642@qq.com
 * desc      消息生产者
 *
 * </pre>
 */
public class Producer {
    
    public static void main(String[] args) {
        Connection conn = null;
        Session session = null;
        try {
            conn = UtilActiveMq.getConn();
            conn.start();
            session = conn.createSession(true, Session.AUTO_ACKNOWLEDGE);
            Destination dest = session.createQueue(UtilActiveMq.QUEUE_NAME);
            MessageProducer producer = session.createProducer(dest);
            List<TextMessage> messages = genMessages(session);
            UtilActiveMq.sendMessages(producer,messages);
            session.commit();
        } catch (JMSException e) {
            e.printStackTrace();
        }finally {
            UtilActiveMq.close(session,conn);
        }
    }

    public static List<TextMessage> genMessages(Session session){
        return Stream.of(new String[]{"1","2"},new String[]{"a","b","c"})
                .flatMap( arr -> Arrays.stream(arr))
                .map(str -> {
                    TextMessage message = null;
                    try {
                        message = session.createTextMessage(str);
                        UtilLogger.println(message.getText());
                    } catch (JMSException e) {
                        e.printStackTrace();
                    }
                    return message;
                })
                .collect(Collectors.toList());
    }

}
